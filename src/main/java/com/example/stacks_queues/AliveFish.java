package com.example.stacks_queues;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AliveFish {

  /**
   * Calculates how many fish finally stay alive. Voracious fish are moving along a river with the
   * same speed. A bigger fish eats the smaller one when they face each other. Each fish has unique
   * position. Complexity O(N ** 2)
   *
   * @param fishSizes In the order of fish downstream positions. All sizes are unique.
   * @param fishDirections In the order of fish downstream positions. 0 - fish is directed upstream,
   *     1 - fish is directed downstream.
   * @return 1 if properly nested, 0 otherwise.
   */
  public static int countAliveFish(int[] fishSizes, int[] fishDirections) {
    // Two fish P and Q meet each other when P < Q, B[P] = 1 and B[Q] = 0, and there are no living
    // fish between them

    List<Integer> sizeList = IntStream.of(fishSizes).boxed().collect(Collectors.toList());
    List<Integer> directionList = IntStream.of(fishDirections).boxed().collect(Collectors.toList());
    boolean hasChanges = true;

    while (hasChanges) {
      hasChanges = false;
      List<Integer> newSizeList = new ArrayList<>();
      List<Integer> newDirectionList = new ArrayList<>();
      int i = 0;
      for (; i < sizeList.size(); i++) {
        boolean hasNextFish = i + 1 < sizeList.size();
        if (!hasNextFish) {
          boolean hasPreviousFish = i - 1 >= 0;
          if (!hasPreviousFish) {
            return 1;
          }
          Integer aFishSize = sizeList.get(i - 1);
          Integer bFishSize = sizeList.get(i);
          Integer aFishDirection = directionList.get(i - 1);
          Integer bFishDirection = directionList.get(i);

          boolean lastFishWasEaten =
              bFishSize < aFishSize && aFishDirection == 1 && bFishDirection == 0;

          if (!lastFishWasEaten && !newSizeList.contains(bFishSize)) {
            newSizeList.add(bFishSize);
            newDirectionList.add(bFishDirection);
          }
          break;
        }
        int aFishDirection = directionList.get(i);
        int bFishDirection = directionList.get(i + 1);
        int aFishSize = sizeList.get(i);
        int bFishSize = sizeList.get(i + 1);
        boolean facing = aFishDirection == 1 && bFishDirection == 0;
        if (!facing) {
          newSizeList.add(aFishSize);
          newDirectionList.add(aFishDirection);
          continue;
        }
        boolean aEatsB = aFishSize > bFishSize;
        if (aEatsB) {
          newSizeList.add(aFishSize);
          newDirectionList.add(aFishDirection);
          i++;
          hasChanges = true;
        } else {
          newSizeList.add(bFishSize);
          newDirectionList.add(bFishDirection);
          hasChanges = true;
        }
      }
      sizeList = newSizeList;
      directionList = newDirectionList;
    }
    return sizeList.size();
  }
}
