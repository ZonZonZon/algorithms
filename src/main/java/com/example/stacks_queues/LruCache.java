package com.example.stacks_queues;

public interface LruCache {

  void put(int key, int value);

  Integer get(int key);
}
