package com.example.stacks_queues;

import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Implementation of a fixed size cache that clears its oldest value when size is exceeded. Also
 * reading and adding makes a value the most recently used one.
 */
public class LruCacheImpl implements LruCache {

  private int cacheSize;
  private Map<Integer, Integer> cache = new LinkedHashMap<>();
  private Deque<Integer> queue;

  public LruCacheImpl(int cacheSize) {
    this.queue = new LinkedBlockingDeque<>(cacheSize);
    this.cacheSize = cacheSize;
  }

  @Override
  public Integer get(int key) {
    Integer value = cache.get(key);
    if (value == null) {
      return null;
    }
    cache.remove(key);
    queue.remove(key);
    put(key, value);
    return value;
  }

  @Override
  public void put(int key, int value) {
    if (cache.size() >= cacheSize) {
      Integer lastOne = queue.removeLast();
      cache.remove(lastOne);
    }
    cache.put(key, value);
    queue.push(key);
  }
}
