package com.example.stacks_queues;

public class GroceryStore {

  /**
   * Counts the size of queue operations, when the grocery queue first becomes empty.
   *
   * @param queueOperations Operation 0 - a person joins the line. Operation 1 - a person is served
   *     and leaves the line.
   */
  public static int getMinOperations(int[] queueOperations) {
    int count = 0;
    int queueSize = 0;
    for (int queueOperation : queueOperations) {
      queueSize += queueOperation == 1 ? -1 : 1;
      count++;
      if (queueSize <= 0) {
        return count;
      }
    }
    return Integer.MAX_VALUE;
  }
}
