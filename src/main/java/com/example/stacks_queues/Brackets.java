package com.example.stacks_queues;

import java.util.ArrayDeque;
import java.util.Deque;

public class Brackets {

  /**
   * Determine whether a given string of parentheses (multiple types) is properly nested. Properly
   * nested is either 1) Text is empty, 2) Properly nested part is wrapped into brackets of (), {}
   * or [], 3) Text is a concatenation of properly nested parts. Complexity O(3**N).
   *
   * @return 1 if properly nested, 0 otherwise.
   */
  public static int isProperlyNested2(String text) {
    Deque<Character> stack = new ArrayDeque<>();

    for(int i = 0; i < text.length(); i++) {
      char character = text.charAt(i);

      switch (character) {
        case ')':
          if (stack.isEmpty() || stack.pop() != '(')
            return 0;
          break;
        case ']':
          if (stack.isEmpty() || stack.pop() != '[')
            return 0;
          break;
        case '}':
          if(stack.isEmpty() || stack.pop() != '{')
            return 0;
          break;
        default:
          stack.push(character);
          break;
      }
    }

    return stack.isEmpty() ? 1 : 0;
  }
}
