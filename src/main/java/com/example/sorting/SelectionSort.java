package com.example.sorting;

public class SelectionSort {

  /**
   * The first element is swapped with the smaller from the rest of the array. This is repeated for
   * every other array position. Complexity - O(n^2).
   */
  public static int[] sortAscending(int[] intArray) {
    for (int i = 0; i < intArray.length - 1; i++) {
      int minElementIndex = i;
      for (int j = i + 1; j < intArray.length; j++) {
        if (intArray[minElementIndex] > intArray[j]) {
          minElementIndex = j;
        }
      }

      if (minElementIndex != i) {
        int temp = intArray[i];
        intArray[i] = intArray[minElementIndex];
        intArray[minElementIndex] = temp;
      }
    }
    return intArray;
  }

  public static int[] sortDescending(int[] intArray) {
    for (int i = 0; i < intArray.length - 1; i++) {
      int maxElementIndex = i;
      for (int j = i + 1; j < intArray.length; j++) {
        if (intArray[maxElementIndex] < intArray[j]) {
          maxElementIndex = j;
        }
      }

      if (maxElementIndex != i) {
        int temp = intArray[i];
        intArray[i] = intArray[maxElementIndex];
        intArray[maxElementIndex] = temp;
      }
    }
    return intArray;
  }
}
