package com.example.sorting;

public class ShellSort {

  /**
   * Rearrange elements at each n/2, n/4, n/8 intervals from iterated element. Complexity O(nlog n).
   */
  public static int[] sortAscending(int[] intArray) {
    for (int interval = intArray.length / 2; interval > 0; interval /= 2) {
      for (int i = interval; i < intArray.length; i += 1) {
        int temp = intArray[i];
        int j;
        for (j = i; j >= interval && intArray[j - interval] > temp; j -= interval) {
          intArray[j] = intArray[j - interval];
        }
        intArray[j] = temp;
      }
    }
    return intArray;
  }
}
