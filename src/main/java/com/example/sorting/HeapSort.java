package com.example.sorting;

public class HeapSort {

  /**
   * Relays array into a tree and makes each node value be bigger than its descendants. Complexity
   * O(nlog n).
   */
  public static int[] sortAscending(int[] intArray) {
    // Build max heap:
    for (int i = intArray.length / 2 - 1; i >= 0; i--) {
      heapify(intArray, intArray.length, i);
    }
    // Heap sorting:
    for (int i = intArray.length - 1; i >= 0; i--) {
      int temp = intArray[0];
      intArray[0] = intArray[i];
      intArray[i] = temp;
      // Heapify root element:
      heapify(intArray, i, 0);
    }
    return intArray;
  }

  static void heapify(int[] intArray, int size, int nodeIndex) {
    // Find largest among root, left child and right child:
    int largestNodeIndex = nodeIndex;
    int leftNode = 2 * nodeIndex + 1;
    int rightNode = 2 * nodeIndex + 2;
    if (leftNode < size && intArray[leftNode] > intArray[largestNodeIndex]) {
      largestNodeIndex = leftNode;
    }
    if (rightNode < size && intArray[rightNode] > intArray[largestNodeIndex]) {
      largestNodeIndex = rightNode;
    }
    // Swap and continue heapifying if root is not largest:
    if (largestNodeIndex != nodeIndex) {
      int swap = intArray[nodeIndex];
      intArray[nodeIndex] = intArray[largestNodeIndex];
      intArray[largestNodeIndex] = swap;
      heapify(intArray, size, largestNodeIndex);
    }
  }
}
