package com.example.sorting;

public class CountingSort {

  /** Sorts counting occurrences of each value. Complexity O(n+m). */
  public static int[] sortAscending(int[] intArray) {
    int size = intArray.length;
    int[] outputArray = new int[size + 1];

    // Find the largest element of the array:
    int max = intArray[0];
    for (int i = 1; i < size; i++) {
      if (intArray[i] > max) max = intArray[i];
    }
    int[] countArray = new int[max + 1];

    // Initialize count array with all zeroes:
    for (int i = 0; i < max; ++i) {
      countArray[i] = 0;
    }

    // Store the count of each element:
    for (int j : intArray) {
      countArray[j]++;
    }

    // Store the cumulative count of each array:
    for (int i = 1; i <= max; i++) {
      countArray[i] += countArray[i - 1];
    }

    // Find the index of each element of the original array in count array, and
    // place the elements in output array:
    for (int i = size - 1; i >= 0; i--) {
      outputArray[countArray[intArray[i]] - 1] = intArray[i];
      countArray[intArray[i]]--;
    }

    // Copy the sorted elements into original array:
    System.arraycopy(outputArray, 0, intArray, 0, size);

    return intArray;
  }
}
