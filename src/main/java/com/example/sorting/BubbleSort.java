package com.example.sorting;

public class BubbleSort {

  /** Sorts comparing neighbours. Complexity O(n2). */
  static int[] sortAscending(int[] intArray) {
    int size = intArray.length;
    // Loop through each element:
    for (int i = 0; i < (size - 1); i++) {
      boolean hasSwapped = false;
      // Loop to compare adjacent elements:
      for (int j = 0; j < (size - i - 1); j++) {
        // Compare two array elements:
        if (intArray[j] > intArray[j + 1]) {
          // Swap if elements order is incorrect:
          int temp = intArray[j];
          intArray[j] = intArray[j + 1];
          intArray[j + 1] = temp;
          hasSwapped = true;
        }
      }
      // No swapping needed - the array is sorted:
      if (!hasSwapped) {
        break;
      }
    }
    return intArray;
  }
}
