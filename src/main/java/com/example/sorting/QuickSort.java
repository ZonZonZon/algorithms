package com.example.sorting;

public class QuickSort {

  /**
   * Performs a quick sorting comparing elements with a pivot element and swapping them. Complexity
   * O(n*log n).
   */
  public static int[] sortAscending(int[] intArray, int small, int big) {
    if (small < big) {
      // Find pivot element. Move smaller left and bigger right:
      int pivotElement = subSort(intArray, small, big);
      // Sort array on the left from the pivot element:
      sortAscending(intArray, small, pivotElement - 1);
      // Sort array on the right from the pivot element:
      sortAscending(intArray, pivotElement + 1, big);
    }
    return intArray;
  }

  private static int subSort(int[] intArray, int small, int big) {
    // Choose the right-most as the pivot element:
    int pivot = intArray[big];
    // Pointer to the biggest element:
    int i = (small - 1);
    // Loop through each and compare with the pivot element:
    for (int j = small; j < big; j++) {
      if (intArray[j] <= pivot) {
        // Swap an element smaller than the pivot with the biggest one:
        i++;
        int temp = intArray[i];
        intArray[i] = intArray[j];
        intArray[j] = temp;
      }
    }
    // Swap the pivot element with the biggest:
    int temp = intArray[i + 1];
    intArray[i + 1] = intArray[big];
    intArray[big] = temp;
    // Return the position from where array partition is done:
    return (i + 1);
  }
}
