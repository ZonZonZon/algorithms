package com.example.sorting;

import java.util.Arrays;

public class DistinctValues {

  /** Returns number of distinct values in an array. Complexity O(N*log(N)) or O(N). */
  static int countDistinct(int[] intArray) {
    if (intArray.length == 0) {
      return 0;
    }
    int max = Arrays.stream(intArray).max().orElseThrow();
    int min = Arrays.stream(intArray).min().orElseThrow();
    boolean hasNegatives = min < 0;
    min = hasNegatives ? Math.abs(min) : 0;
    int[] distinctArray = new int[min + max + 1];
    for (int value : intArray) {
      if (value < 0) {
        // Put a negative into negatives range:
        distinctArray[min-Math.abs(value)] = 1;
      } else {
        // Put a positive into positives range:
        distinctArray[min + value] = 1;
      }
    }
    return Arrays.stream(distinctArray).sum();
  }
}
