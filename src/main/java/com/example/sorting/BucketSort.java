package com.example.sorting;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Sorts by grouping values into buckets. Each bucket contains values in a range. Each group values
 * are sorted separately and then results are combined. Complexity O(n).
 */
public class BucketSort {
  public static int[] sortAscending(int[] intArray, int min, int max) {
    int bucketsCount = (max - min) / 3;
    bucketsCount = max * 1.0 / bucketsCount % bucketsCount > 0 ? bucketsCount + 1 : bucketsCount;
    @SuppressWarnings("unchecked")
    ArrayList<Integer>[] bucket = new ArrayList[bucketsCount];
    // Create empty buckets:
    for (int i = 0; i < bucketsCount; i++) {
      bucket[i] = new ArrayList<>();
    }
    // Distribute elements among the buckets:
    for (int value : intArray) {
      int bucketIndex = bucketsCount - (max / value) - 1;
      bucketIndex = Math.min(bucketIndex, bucketsCount - 1);
      bucketIndex = Math.max(bucketIndex, 0);
      bucket[bucketIndex].add(value);
    }
    // Sort the elements of each bucket:
    for (int i = 0; i < bucketsCount; i++) {
      Collections.sort((bucket[i]));
    }
    // Get the sorted array
    int index = 0;
    for (int i = 0; i < bucketsCount; i++) {
      for (int j = 0, size = bucket[i].size(); j < size; j++) {
        intArray[index++] = bucket[i].get(j);
      }
    }
    return intArray;
  }
}
