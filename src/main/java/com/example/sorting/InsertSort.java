package com.example.sorting;

public class InsertSort {

  /**
   * Sorts by splitting into sub-arrays, sorting each of them, then doing sorted merging. Complexity
   * is O(n log n).
   */
  public static int[] sortAscending(int[] intArray) {
    int size = intArray.length;

    for (int step = 1; step < size; step++) {
      int key = intArray[step];
      int j = step - 1;

      // Compare key with each element on the left of it until an element smaller than
      // it is found. For descending order, change key<array[j] to key>array[j]:
      while (j >= 0 && key < intArray[j]) {
        intArray[j + 1] = intArray[j];
        --j;
      }

      // Place key at after the element just smaller than it:
      intArray[j + 1] = key;
    }
    return intArray;
  }
}
