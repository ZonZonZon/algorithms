package com.example.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WaveSorting {

  /**
   * Arranges an array of integers in a wave format, where a1 > a2 < a3 > a4 < a5 > a6 < 7 > a8 < a9
   */
  public static Integer[] sortAsWave1(Integer[] intArray) {
    Arrays.sort(intArray);
    List<Integer> result = new ArrayList<>();
    int iLowerIndex = intArray.length - 1;
    for (int i = 0; i <= iLowerIndex - 1; i++) {
      result.add(intArray[iLowerIndex]);
      iLowerIndex--;
      result.add(intArray[i]);
    }
    boolean isOdd = intArray.length % 2 != 0;
    if (isOdd) {
      result.add(intArray[iLowerIndex]);
    }
    return result.toArray(new Integer[0]);
  }
}
