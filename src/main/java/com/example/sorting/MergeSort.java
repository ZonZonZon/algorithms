package com.example.sorting;

public class MergeSort {

  /**
   * Sorts by splitting into sub-arrays, sorting each of them, then doing sorted merging. Complexity
   * is O(n log n).
   */
  public static int[] sortAscending(int[] intArray) {
    return mergeSort(intArray, 0, intArray.length - 1);
  }

  public static int[] merge(int[] intArray, int p, int q, int r) {
    // Create L ← A[p..q] and M ← A[q+1..r]:
    int n1 = q - p + 1;
    int n2 = r - q;
    int L[] = new int[n1];
    int M[] = new int[n2];

    for (int i = 0; i < n1; i++) {
      L[i] = intArray[p + i];
    }

    for (int j = 0; j < n2; j++) {
      M[j] = intArray[q + 1 + j];
    }

    // Maintain current index of sub-arrays and main array:
    int i = 0;
    int j = 0;
    int k = p;

    // Until we reach either end of either L or M, pick larger among
    // elements L and M and place them in the correct position at A[p..r]:
    while (i < n1 && j < n2) {
      if (L[i] <= M[j]) {
        intArray[k] = L[i];
        i++;
      } else {
        intArray[k] = M[j];
        j++;
      }
      k++;
    }

    // When L and M run out of elements, pick up the remaining elements and put in A[p..r]:
    while (i < n1) {
      intArray[k] = L[i];
      i++;
      k++;
    }

    while (j < n2) {
      intArray[k] = M[j];
      j++;
      k++;
    }
    return intArray;
  }

  // Divide the array into two sub-arrays, sort them and merge them:
  public static int[] mergeSort(int[] intArray, int l, int r) {
    if (l < r) {
      // m is the point where the array is divided into two sub-arrays:
      int m = (l + r) / 2;

      mergeSort(intArray, l, m);
      mergeSort(intArray, m + 1, r);

      // Merge the sorted sub-arrays:
      merge(intArray, l, m, r);
    }
    return intArray;
  }
}
