package com.example.sorting;

public class RadixSort {

  /** Sorts numbers by decimal places: smaller to bigger. Complexity O(n+k). */
  public static int[] sortAscending(int[] intArray) {
    int max = getMax(intArray, intArray.length);
    for (int place = 1; max / place > 0; place *= 10) {
      countingSortByPlace(intArray, intArray.length, place);
    }
    return intArray;
  }

  // Sort using counting sorting by digital; place value:
  private static void countingSortByPlace(int[] intArray, int size, int place) {
    int[] output = new int[size + 1];
    int max = intArray[0];
    for (int i = 1; i < size; i++) {
      if (intArray[i] > max) max = intArray[i];
    }
    int[] count = new int[max + 1];

    for (int i = 0; i < max; ++i) count[i] = 0;

    // Calculate count of elements
    for (int i = 0; i < size; i++) count[(intArray[i] / place) % 10]++;

    // Calculate cumulative count
    for (int i = 1; i < max + 1; i++) {
      count[i] += count[i - 1];
    }

    // Place the elements in sorted order
    for (int i = size - 1; i >= 0; i--) {
      output[count[(intArray[i] / place) % 10] - 1] = intArray[i];
      count[(intArray[i] / place) % 10]--;
    }

    if (size >= 0) {
      System.arraycopy(output, 0, intArray, 0, size);
    }
  }

  // Get max array value:
  private static int getMax(int[] intArray, int n) {
    int max = intArray[0];
    for (int i = 1; i < n; i++) {
      if (intArray[i] > max) {
        max = intArray[i];
      }
    }
    return max;
  }
}
