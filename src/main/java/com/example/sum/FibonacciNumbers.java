package com.example.sum;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FibonacciNumbers {

  /**
   * Efficiently gets Fibonacci numbers. Complexity O(n).
   *
   * @param limit A number to stop the Fibonacci sequence.
   */
  public static int[] getFibonacciNumbers(int limit) {
    List<Integer> fibonacciNumbers = new LinkedList<>();
    fibonacciNumbers.add(0);
    fibonacciNumbers.add(1);
    while (fibonacciNumbers.get(fibonacciNumbers.size() - 1) < limit) {
      int last = fibonacciNumbers.get(fibonacciNumbers.size() - 1);
      int preLast = fibonacciNumbers.get(fibonacciNumbers.size() - 2);
      int sum = last + preLast;
      fibonacciNumbers.add(sum);
      fibonacciNumbers.add(last + sum);
    }
    fibonacciNumbers.remove(fibonacciNumbers.size() - 1);
    fibonacciNumbers.remove(fibonacciNumbers.size() - 1);
    int[] result = fibonacciNumbers.stream().mapToInt(integer -> integer).toArray();
    return result;
  }

  /**
   * Finds the value of the Nth Fibonacci number by count. Uses Binet's formula: Sn = Φⁿ–(– Φ⁻ⁿ)/√5
   * where Φ = ( 1 + √5 )/2 = 1.6180339887. Complexity is O(1).
   *
   * @param numberOrder The index of a number to find in a Fibonacci numbers sequence.
   * @return Requested Fibonacci sequence value.
   */
  public static int getNthFibonacciNumber(int numberOrder) {
    double squareRootOf5 = Math.sqrt(5);
    double phi = (1 + squareRootOf5) / 2;

    int nthValue =
        (int) ((Math.pow(phi, numberOrder) - Math.pow(-phi, -numberOrder)) / squareRootOf5);

    return nthValue;
  }

  /**
   * Detects if each number can be a sum of two Fibonacci numbers. Complexity is O(n + m).
   *
   * @param number A number to check weather it can be a sum of any two Fibonacci numbers.
   * @return Fibonacci numbers that sum into the given number.
   */
  public static boolean isFibonacciNumbersSum(int number) {
    int[] fibonacciNumbers = getFibonacciNumbers(number * 2);
    return Arrays.stream(fibonacciNumbers).anyMatch(iterated -> iterated == number);
  }
}
