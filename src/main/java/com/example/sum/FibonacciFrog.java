package com.example.sum;

import java.util.Arrays;

public class FibonacciFrog {

  /**
   * A frog jumps over the river by positions that have leaves. One jump should be of any Fibonacci
   * number length. The starting position is -1, the ending one is over the last river position.
   *
   * @param riverPositions In the index order. True value - if there is a leaf on the position.
   */
  public static int getFibonacciFrogJumps(int[] riverPositions) {
    int currentPositionIndex = -1;
    int jumpsCount = 0;
    // Add last position which is on the bank:
    riverPositions = Arrays.copyOf(riverPositions, riverPositions.length + 1);
    riverPositions[riverPositions.length - 1] = 1;
    for (int i = 0; i < riverPositions.length; i++) {
      boolean hasLeaf = riverPositions[i] == 1;
      boolean isForwardJump = i > currentPositionIndex;
      int jumpLength = i - currentPositionIndex;
      boolean isFibonacciLengthJump = FibonacciNumbers.isFibonacciNumbersSum(jumpLength);
      if (hasLeaf && isForwardJump && isFibonacciLengthJump) {
        jumpsCount++;
        currentPositionIndex = i;
      }
    }
    return jumpsCount == 0 ? -1 : jumpsCount;
  }
}
