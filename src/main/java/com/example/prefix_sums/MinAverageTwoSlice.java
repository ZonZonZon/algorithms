package com.example.prefix_sums;

import java.util.ArrayList;
import java.util.List;

public class MinAverageTwoSlice {

  /**
   * Finds the starting position of an integer array slice whose average is minimal. A slice is a
   * pair of two different indexes in the array. An average is (index1Value + index2Value +
   * indexNValue) / (indexN − index1 + 1). Complexity O(N ** 2)
   *
   * @param intArray A non-empty integer array.
   * @return A starting position of a slice with minimal average. Returns the smallest starting
   *     position if several are matching the minimal value.
   */
  public static int findMinAverageSliceOfTwo(int[] intArray) {
    int[] sums = getNextSumsArray(intArray);
    double minAverage = 10000;
    List<Integer> minAverageIndexes = new ArrayList<>();
    // Loop slice start indexes:
    for (int sliceStartIndex = 0; sliceStartIndex < intArray.length; sliceStartIndex++) {
      // Loop slice end indexes:
      for (int sliceEndIndex = 0; sliceEndIndex < intArray.length; sliceEndIndex++) {
        if (sliceEndIndex <= sliceStartIndex) {
          continue;
        }
        double prefixSum = sliceStartIndex == 0 ? 0 : sums[sliceStartIndex - 1];
        double suffixSum = sums[sums.length - 1] - sums[sliceEndIndex];
        double sliceSum = sums[sums.length - 1] - prefixSum - suffixSum;
        double sliceAverage = sliceSum / (sliceEndIndex - sliceStartIndex + 1);

        if (minAverage >= sliceAverage) {
          //          System.out.println(
          //              "A new min average is detected: "
          //                  + sliceAverage
          //                  + " for a slice "
          //                  + sliceStartIndex
          //                  + ":"
          //                  + sliceEndIndex);
          if (minAverage > sliceAverage) {
            minAverageIndexes.clear();
          }
          if (!minAverageIndexes.contains(sliceStartIndex)) {
            minAverageIndexes.add(sliceStartIndex);
          }
          minAverage = sliceAverage;
        }
      }
    }
    Integer minIndexOfAllMinAverages = minAverageIndexes.stream().sorted().findAny().orElse(10000);
    return minIndexOfAllMinAverages;
  }

  /**
   * Sums after index (excludes first value on 0 and excludes the last one).
   */
  public static int[] getNextSumsArray(int[] intArray) {
    // Count accumulating sums:
    int[] sums = new int[intArray.length];
    for (int i = 0; i < intArray.length; i++) {
      int previousSum = i == 0 ? 0 : sums[i - 1];
      sums[i] = previousSum + intArray[i];
    }
    return sums;
  }
}
