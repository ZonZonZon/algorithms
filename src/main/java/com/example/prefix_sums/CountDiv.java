package com.example.prefix_sums;

public class CountDiv {

  /**
   * Counts the number of integers divisible by divider in range [rangeStart..rangeEnd]. It is
   * counted when remainder is 0.
   */
  public static int countDivisibleIntegers(int rangeStart, int rangeEnd, int divider) {
    int divisors = 0;
    // Ignore on divider > rangeEnd:
    if (divider > rangeEnd) {
      return rangeStart == 0 || rangeEnd == 0 ? 1 : 0;
    }
    if (rangeStart == 0) {
      divisors++;
    }
    int rangeStartBeloweDivider = Math.max(divider, rangeStart);
    int firstNonZeroDivisor = 0;
    int lastNonZeroDivisor = 0;
    for (int i = rangeStartBeloweDivider; i <= rangeEnd; i++) {
      if (i % divider == 0) {
        firstNonZeroDivisor = i;
        break;
      }
    }
    for (int i = rangeEnd; i >= rangeStartBeloweDivider; i--) {
      if (i % divider == 0) {
        lastNonZeroDivisor = i;
        break;
      }
    }
    if (firstNonZeroDivisor == 0 && lastNonZeroDivisor == 0) {
      divisors = 0;
    } else {
      divisors += ((lastNonZeroDivisor - firstNonZeroDivisor) / divider) + 1;
    }

    return divisors;
  }
}
