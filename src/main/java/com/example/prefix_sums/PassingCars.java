package com.example.prefix_sums;

public class PassingCars {

  /**
   * Count the number of passing cars on the road. Cars are passing by when they are moving in the
   * opposite directions and opposite cars are not behind in the array. Only cars that move towards
   * each other are counted: West & East are not, but East & West are.
   *
   * @param passingCars Array of passing cars where 0 goes East and 1 goes West. Cars earlier in the
   *     array have already passed and are behind.
   * @return Number of pairs of cars passing by.
   */
  public static int countOneDirectionPassingByPairs(int[] passingCars) {
    if (passingCars.length > 1000000) {
      return -1;
    }
    // Calculating for one-direction cars only:
    int[] oppositeCarSums = new int[passingCars.length];
    boolean firstEasternCarFound = false;
    for (int i = 0; i < passingCars.length; i++) {
      if (!firstEasternCarFound && passingCars[i] == 0) {
        // Start counting:
        firstEasternCarFound = true;
      }
      if (firstEasternCarFound && passingCars[i] == 1) {
        // Count a facing car:
        oppositeCarSums[i] += oppositeCarSums[i - 1] + 1;
      } else if (i - 1 >= 0) {
        // Copy previous sum value:
        oppositeCarSums[i] += oppositeCarSums[i - 1];
      }
    }
    int pairsCount = 0;
    for (int i = 0; i < passingCars.length; i++) {
      if (passingCars[i] == 0) {
        // Count all facing cars for this Eastern car:
        pairsCount += oppositeCarSums[oppositeCarSums.length - 1] - oppositeCarSums[i];
      }
    }
    return pairsCount;
  }

  /**
   * Count the number of any cars moving in the opposite direction, excluding those behind in the
   * array. It is considered that cars moving West & East meet each other.
   *
   * @param passingCars Array of passing cars where 0 goes East and 1 goes West. Cars earlier in the
   *     array have already passed and are behind.
   * @return Number of pairs of cars passing by.
   */
  public static int countAllPassingByPairs(int[] passingCars) {
    if (passingCars.length > 1000000) {
      return -1;
    }
    // Calculating for one-direction cars only:
    int oppositeCarType = passingCars[0] == 0 ? 1 : 0;
    boolean isToCountZeroes = oppositeCarType == 0;
    int[] oppositeCarSums = new int[passingCars.length];
    for (int i = 0; i < passingCars.length; i++) {
      int passingCarCount = 0;
      if (!isToCountZeroes && passingCars[i] == 1) {
        passingCarCount = 1;
      }
      if (isToCountZeroes && passingCars[i] == 0) {
        passingCarCount = 1;
      }
      oppositeCarSums[i] = i - 1 >= 0 ? oppositeCarSums[i - 1] + passingCarCount : passingCarCount;
    }
    int pairsCounter = 0;
    for (int i = 0; i < passingCars.length; i++) {
      int passingCar = passingCars[i];

      pairsCounter +=
          passingCar != oppositeCarType
              ? oppositeCarSums[oppositeCarSums.length - 1] - oppositeCarSums[i]
              : 0;
    }
    return pairsCounter;
  }
}
