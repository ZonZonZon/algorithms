package com.example.prefix_sums;

import java.util.Arrays;

public class GenomicRangeQuery {

  /**
   * Find the minimal nucleotide from a range of DNA sequence. DNA letters are A, C, G and T having
   * impact 1, 2 3, and 4 respectively. Queries are given to test DNA: P & Q. A query takes DNA
   * letters from P index to Q index and finds min impacted one. Complexity here is O(N * M).
   *
   * @return Each test query result.
   */
  public static int[] getDnaMinimalImpacts(String dnaSequence, int[] pQueries, int[] qQueries) {
    int[] minImpacts = new int[pQueries.length];
    for (int i = 0; i < pQueries.length; i++) {
      String queryExtract = dnaSequence.substring(pQueries[i], qQueries[i] + 1);

      minImpacts[i] =
          Arrays.stream(queryExtract.split(""))
              .sorted()
              .findFirst()
              .map(GenomicRangeQuery::getImpact)
              .stream()
              .findAny()
              .orElse(99999);
    }
    return minImpacts;
  }

  private static int getImpact(String dnaLetter) {
    switch (dnaLetter) {
      case "A":
        return 1;
      case "C":
        return 2;
      case "G":
        return 3;
      case "T":
        return 4;
      default:
        return 0;
    }
  }
}
