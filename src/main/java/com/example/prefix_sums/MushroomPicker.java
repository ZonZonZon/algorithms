package com.example.prefix_sums;

public class MushroomPicker {

  /**
   * A picker moves to the nearest spot to collect all the mushrooms there. She can visit a spot the
   * second time, but it will be empty. She can move any direction: back and forth.
   *
   * @param pickerStartIndex Where the picker starts his way.
   * @param maxPickerMoves The max way moves to perform.
   * @param mushroomsPlaces A sequence of places with amount of mushrooms.
   * @return The maximum number of mushrooms that the picker can collect with given moves.
   */
  public static int collectMushrooms(
      int pickerStartIndex, int maxPickerMoves, int[] mushroomsPlaces) {

    int[] cumulativeSums = new int[mushroomsPlaces.length];

    // Calculate cumulative sums:
    for (int i = 0; i < mushroomsPlaces.length; i++) {
      cumulativeSums[i] = i > 0 ? cumulativeSums[i - 1] + mushroomsPlaces[i] : mushroomsPlaces[i];
    }
    int minLeftIndex =
        pickerStartIndex - maxPickerMoves >= 0 ? pickerStartIndex - maxPickerMoves : 0;

    int maxRightIndex = Math.min(pickerStartIndex + maxPickerMoves, mushroomsPlaces.length - 1);

    int maxMushrooms = 0;
    // Iterate slice positions from maxLeft & minRight to minLeftIndex & maxRightIndex:
    for (int leftIndex = minLeftIndex, rightIndex = minLeftIndex + maxPickerMoves;
        rightIndex <= maxRightIndex;
        leftIndex++, rightIndex++) {

      // Detect move boundaries based on moves depth left:
      int movesLeft = (pickerStartIndex - leftIndex) / 2;
      int movesRight = Math.min(maxRightIndex - pickerStartIndex, maxPickerMoves - movesLeft * 2);

      int arrayPrefixSum =
          pickerStartIndex - movesLeft - 1 >= 0
              ? cumulativeSums[pickerStartIndex - movesLeft - 1]
              : 0;

      int arraySuffixSum =
          cumulativeSums[cumulativeSums.length - 1]
              - (pickerStartIndex + movesRight + 1 <= mushroomsPlaces.length - 1
                  ? cumulativeSums[pickerStartIndex + movesRight]
                  : cumulativeSums[cumulativeSums.length - 1]);

      int arraySum = cumulativeSums[cumulativeSums.length - 1] - arrayPrefixSum - arraySuffixSum;
      maxMushrooms = Math.max(arraySum, maxMushrooms);
    }
    return maxMushrooms;
  }
}
