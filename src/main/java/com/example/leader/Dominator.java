package com.example.leader;

public class Dominator {

  /**
   * Find an index of an array such that its value occurs at more than half of indices in the array.
   *
   * @return Any index of the leader occurrence. Otherwise -1.
   */
  public static int getMostFrequentIndex(int[] intArray) {
    int leaderIndex = -1;
    int stackCounter = 0;
    int leader = -1;
    int currentValue = -1;

    for (int i = 0; i < intArray.length; i++) {
      int intValue = intArray[i];
      if (stackCounter == 0) {
        currentValue = intValue;
        ++stackCounter;
        leaderIndex = i;
      } else {
        if (currentValue == intValue) {
          ++stackCounter;
        } else {
          --stackCounter;
        }
      }
    }

    if (stackCounter > 0) {
      leader = currentValue;
    } else {
      return -1;
    }

    int leaderOccurences = 0;
    for (int element : intArray) {
      if (element == leader) {
        ++leaderOccurences;
      }
      if (leaderOccurences > (intArray.length / 2)) {
        return leaderIndex;
      }
    }
    return -1;
  }
}
