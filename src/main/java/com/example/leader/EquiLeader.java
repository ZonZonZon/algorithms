package com.example.leader;

import static org.assertj.core.util.Arrays.asList;

import java.util.Arrays;
import java.util.Collections;

public class EquiLeader {

  /**
   * Find the index after which array can be cut into two parts, such that each part will have the
   * same leader (dominating value). Complexity .
   */
  public static int getEquiLeader(int[] intArray) {
    int equiLeadersCount = 0;
    for (int i = 1; i < intArray.length; i++) {
      int[] aArray = Arrays.copyOfRange(intArray, 0, i);
      Integer aLeader = getLeader(aArray);
      int[] bArray = Arrays.copyOfRange(intArray, i, intArray.length);
      Integer bLeader = getLeader(bArray);
      if (aLeader != null && aLeader.equals(bLeader)) {
        equiLeadersCount++;
      }
    }
    return equiLeadersCount;
  }

  /** Gets leader by sorting, taking middle number, and counting its occurances. */
  private static Integer getLeader(int[] array) {
    Arrays.sort(array);
    Integer leader;
    int middleIndex = array.length / 2;
    int middleNumber = array[middleIndex];
    if (middleIndex == 0) {
      return middleNumber;
    }
    int leaderCount = Collections.frequency(asList(array), middleNumber);
    leader = leaderCount > array.length / 2 ? middleNumber : null;
    return leader;
  }
}
