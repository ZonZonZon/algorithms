package com.example.dynamic;

public class CoinsExchange {

  /**
   * Finds the minimum number of coins with which a given amount of money can be paid. Improves a
   * greedy algorithm solution by iterating different denomination combinations. Complexity О(n *
   * k).
   */
  public static int getMinCoinsCombination(int[] deniminations, int sum) {
    int totalCoins = deniminations.length;
    // Create the multidimensional array: subTask - subResults:
    double[][] subResults = new double[totalCoins + 1][sum + 1];
    for (int subResultIndex = 0; subResultIndex <= sum; subResultIndex++) {
      subResults[0][subResultIndex] = Double.POSITIVE_INFINITY;
    }
    for (int subTaskIndex = 1; subTaskIndex <= totalCoins; subTaskIndex++) {
      subResults[subTaskIndex][0] = 0;
    }
    // Fill the array:
    for (int coinsCount = 1; coinsCount <= totalCoins; coinsCount++) {
      // For each subtask with different coins amount:
      for (int currentSum = 1; currentSum <= sum; currentSum++) {
        // Get optimal set of denominations to get the sum:
        if (deniminations[coinsCount - 1] <= currentSum) {
          subResults[coinsCount][currentSum] =
              Math.min(
                  1 + subResults[coinsCount][currentSum - deniminations[coinsCount - 1]],
                  subResults[coinsCount - 1][currentSum]);
        } else {
          subResults[coinsCount][currentSum] = subResults[coinsCount - 1][currentSum];
        }
      }
    }
    return (int) subResults[totalCoins][sum];
  }
}
