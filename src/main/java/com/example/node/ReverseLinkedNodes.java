package com.example.node;

public class ReverseLinkedNodes {

  public static class Node {

    private int value;
    private Node next;

    public Node(Integer value, Node next) {
      this.value = value;
      this.next = next;
    }

    public void setValue(int value) {
      this.value = value;
    }

    public void setNext(Node next) {
      this.next = next;
    }

    public int getValue() {
      return value;
    }

    public Node getNext() {
      return next;
    }

    public static Node createNode(Integer value, Node next) {
      Node nextNode = new Node(value, next);
      return nextNode;
    }

    public static Node reverse(Node node) {
      Node currentReadNode = node.getNext();
      Node currentWriteNode = new Node(node.getValue(), null);
      while (currentReadNode != null) {
        currentWriteNode = createNode(currentReadNode.getValue(), currentWriteNode);
        currentReadNode = currentReadNode.getNext();
      }
      return currentWriteNode;
    }
  }
}
