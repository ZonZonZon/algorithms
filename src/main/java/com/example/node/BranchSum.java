package com.example.node;

import java.util.Objects;
import java.util.stream.Stream;

public class BranchSum {

  /** Finds required sum of linked tree nodes from root to the leaf (childless node). */
  public static boolean hasSum(TreeNode root, int sum) {
    if (root == null) {
      return false;
    }
    if (root.left == null && root.right == null) {
      return false;
    }
    Integer finalSum = getSum(root, root.value, sum);
    return finalSum != null && finalSum == sum;
  }

  private static Integer getSum(TreeNode root, int currentSum, int targetSum) {
    if (currentSum > targetSum) {
      return null;
    }
    boolean isLeaf = root.left == null &&  root.right == null;
    if (isLeaf && currentSum == targetSum) {
      return currentSum;
    }

    Integer result =
        Stream.of(root.left, root.right)
            .filter(Objects::nonNull)
            .map(treeNode -> getSum(treeNode, currentSum + treeNode.value, targetSum))
            .filter(sum -> sum != null && targetSum == sum)
            .findAny()
            .orElse(null);

    return result;
  }

  public static class TreeNode {
    public TreeNode left;
    public TreeNode right;
    int value;

    TreeNode(int value, TreeNode left, TreeNode right) {
      this.value = value;
      this.left = left;
      this.right = right;
    }
  }
}
