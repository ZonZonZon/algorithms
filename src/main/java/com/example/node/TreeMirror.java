package com.example.node;

public class TreeMirror {

  /**
   * Defines if there is a neighbours sum of any number that is equal to the expected. Complexity is
   * O(n).
   */
  public static TreeNode mirrorDigitsTree(TreeNode root) {
    if (root == null) {
      return null;
    }
    if (root.left == null && root.right == null) {
      return new TreeNode(root.val);
    }
    TreeNode left = mirrorDigitsTree(root.right);
    TreeNode right = mirrorDigitsTree(root.left);
    return new TreeNode(root.val, left, right);
  }

  public static class TreeNode {
    public TreeNode left;
    public TreeNode right;
    int val;
    TreeNode(int val) {
      this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
      this.val = val;
      this.left = left;
      this.right = right;
    }
  }
}
