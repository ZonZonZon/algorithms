package com.example.complexity;

public class FrogJumps {

  /** Counts minimal amount of frog jumps of D length from X to Y position. */
  public static int countMinFrogJumps(int startPositionX, int finishPositionY, int jumpLengthD) {
    int routeLength = finishPositionY - startPositionX;
    if (startPositionX == finishPositionY) {
      return 0;
    }
    int remainder = routeLength % jumpLengthD;
    int minJumpsCount = (routeLength - remainder) / jumpLengthD + (remainder == 0 ? 0 : 1);
    return minJumpsCount;
  }
}
