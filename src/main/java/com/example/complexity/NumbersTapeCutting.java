package com.example.complexity;

import java.util.Arrays;
import java.util.stream.IntStream;

public class NumbersTapeCutting {

  /** Finds a missing consecutive integer in the array. Integer order in the array is random. */
  public static int leftAndRightSumsMinDifference(int[] intArray) {
    int minDifference = Integer.MAX_VALUE;
    int[] sortedArray = Arrays.copyOf(intArray, intArray.length);
    Arrays.sort(sortedArray);
    int totalSum = IntStream.of(intArray).sum();
    int sumLeft = 0;
    int sumRight = 0;
    for (int i = 0; i < intArray.length - 1; i++) {
      int number = intArray[i];
      sumLeft += number;
      sumRight = totalSum - sumLeft;
      int difference = Math.abs(sumLeft - sumRight);
      minDifference = Math.min(difference, minDifference);
    }
    return minDifference;
  }
}
