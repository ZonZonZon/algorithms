package com.example.complexity;

import java.util.Arrays;

public class FindMissingElement {

  /** Finds a missing consecutive integer in the array. Integer order in the array is random. */
  public static int findMissingElement1(int[] intArray) {
    Arrays.sort(intArray);
    for (int i = 0; i < intArray.length; i++) {
      Integer current = intArray[i];
      Integer next = i + 1 <= intArray.length - 1 ? intArray[i + 1] : null;
      if (next != null && next - current > 1) {
        return next - 1;
      }
    }

    return intArray.length == 0 ? 1 : intArray[intArray.length -1] + 1;
  }
}
