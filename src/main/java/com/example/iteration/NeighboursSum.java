package com.example.iteration;

public class NeighboursSum {

  /**
   * Defines if there is a neighbours sum of any number that is equal to the expected. Complexity is
   * O(n).
   */
  public static boolean hasNeighboursSum(int[] elements, int target) {
    for (int i = 0; i < elements.length; i++) {
      Integer sum = null;
      if (i > 2) {
        sum = elements[i] + elements[i - 1] + elements[i - 2];
      }
      if (sum != null && sum == target) {
        return true;
      }
    }
    return false;
  }
}
