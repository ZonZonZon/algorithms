package com.example.iteration;

import java.util.PrimitiveIterator.OfInt;

public class BinaryGap {

  /**
   * Find the longest sequence of zeros in binary representation of an integer. Sequence should be
   * wrapped with '1'.
   *
   * @param n A given integer.
   * @return The max zeroes sequence count.
   */
  public static int getMaxBinaryGap1(int n) {
    final Character one = '1';
    final Character zero = '0';
    String binaryString = Integer.toBinaryString(n);
    int maxCounter = 0;
    int currentCounter = 0;
    Character previousChar = null;
    boolean isClosed;
    boolean isOpen = false;
    OfInt iterator = binaryString.chars().iterator();
    while (iterator.hasNext()) {
      int currentCharCode = iterator.next();
      Character currentChar = (char) currentCharCode;
      isClosed = previousChar != null && previousChar.equals(zero) && currentChar.equals(one);
      isOpen =
          (previousChar != null && previousChar.equals(one) && currentChar.equals(zero))
              || (isOpen && currentChar.equals(zero));
      // Update max counter:
      if (isClosed) {
        if (currentCounter > maxCounter) {
          maxCounter = currentCounter;
        }
        currentCounter = 0;
      }
      // Count:
      if (isOpen) {
        currentCounter++;
      }
      previousChar = currentChar;
    }
    return maxCounter;
  }

  /**
   * Find the longest sequence of zeros in binary representation of an integer. Sequence should be
   * wrapped with '1'.
   *
   * @param n A given integer.
   * @return The max zeroes sequence count.
   */
  public static int getMaxBinaryGap2(int n) {
    String binaryString = Integer.toBinaryString(n);
    int binaryLength = binaryString.toCharArray().length;
    StringBuilder pattern = new StringBuilder().append("0");
    int patternLength = 1;
    int maxCounter = 0;
    while (binaryLength >= patternLength) {
      if (binaryString.contains(pattern.toString())) {
        maxCounter = patternLength;
      }
      pattern.append(pattern.substring(pattern.length() - 1));
      patternLength++;
    }
    return maxCounter;
  }
}
