package com.example.slice;

public class MaxDoubleSlice {

  /**
   * Based on bidirectional Kadane's algorithm. Find the maximal sum (special calculations) of any
   * double slice. Double slice is a triplet (X, * Y, Z), such that indexes are 0 ≤ X < Y < Z <
   * array size. Complexity O(N).
   */
  public static int getDoubleSliceMaxSum(int[] intArray) {
    int[] array1 = new int[intArray.length];
    int[] array2 = new int[intArray.length];
    // Maximum sum contiguous subsequence ending with index i:
    for (int i = 1; i < intArray.length - 1; i++) {
      array1[i] = Math.max(array1[i - 1] + intArray[i], 0);
    }
    // Maximum sum contiguous subsequence starting with index i:
    for (int i = intArray.length - 2; i > 0; i--) {
      array2[i] = Math.max(array2[i + 1] + intArray[i], 0);
    }
    int max = 0;
    for (int i = 1; i < intArray.length - 1; i++) {
      max = Math.max(max, array1[i - 1] + array2[i + 1]);
    }
    return max;
  }

  /**
   * Find the maximal sum (special calculations) of any double slice. Double slice is a triplet (X,
   * Y, Z), such that indexes are 0 ≤ X < Y < Z < array size. Complexity O(N ** 3).
   */
  public static int getDoubleSliceMaxSum2(int[] intArray) {
    int[] sums = getSumsArray(intArray);
    int maxSum = -10000;
    for (int xIndex = 0; xIndex < sums.length - 2; xIndex++) {
      // Shift X:
      if (xIndex + 1 < sums.length) {
        // Shift Y:
        for (int yIndex = xIndex + 1; yIndex < sums.length; yIndex++) {
          if (yIndex + 1 < sums.length) {
            // Shift Z:
            for (int zIndex = yIndex + 1; zIndex < sums.length; zIndex++) {
              int sum = countSum(intArray, sums, xIndex, yIndex, zIndex);
              if (sum > maxSum) {
                maxSum = sum;
              }
            }
          }
        }
      }
    }
    return maxSum;
  }

  /** Relays array into sums array, such that number indexes match the same sum indexes. */
  public static int[] getSumsArray(int[] intArray) {
    int[] sums = new int[intArray.length];
    for (int i = 0; i < intArray.length; i++) {
      if (i == 0) {
        sums[i] = intArray[i];
        continue;
      }
      sums[i] = intArray[i] + sums[i - 1];
    }
    return sums;
  }

  /**
   * The sum of double slice (X, Y, Z) is the total of positive values of A[X+1] + A[X+2] + ... +
   * A[Y−1] + A[Y+1] + A[Y+2] + ... + A[Z−1]. So sum until next triplet element is met. One number
   * can be summed only once.
   */
  public static int countSum(int[] intArray, int[] sums, int xIndex, int yIndex, int zIndex) {
    int sum = -10000;
    if (xIndex + 1 < intArray.length && yIndex < intArray.length && zIndex < intArray.length) {
      int totalSum = sums[sums.length - 1];
      int xPrefixSum = sums[xIndex];
      int zSuffixSum = totalSum - sums[zIndex - 1];
      sum = totalSum - xPrefixSum - zSuffixSum - intArray[yIndex];
    }
    return sum;
  }
}
