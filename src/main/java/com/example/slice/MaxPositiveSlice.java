package com.example.slice;

public class MaxPositiveSlice {

  /**
   * Calculates the biggest positive sum of consecutive slice elements. 0 if all are negative.
   */
  public static int getMaxSlice(int[] intArray) {
      int nextSum = 0;
      int maxSum = 0;
      for (int number : intArray) {
        nextSum = Math.max(0, nextSum + number);
        maxSum = Math.max(maxSum, nextSum);
      }
      return maxSum;
  }
}
