package com.example.slice;

public class MaxSlice {

  /** Calculates the biggest sum of consecutive slice elements. */
  public static int getMaxSlice(int[] intArray) {
    boolean hasStarted = false;
    int nextSum = intArray.length > 0 ? intArray[0] : 0;
    int maxSum = nextSum;
    for (int number : intArray) {
      nextSum = Math.max(number, hasStarted ? nextSum + number : number);
      maxSum = Math.max(maxSum, nextSum);
      hasStarted = true;
    }
    return maxSum;
  }
}
