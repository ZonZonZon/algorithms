package com.example.slice;

public class CaterpillarMethod {

  /** Finds subslice by a sum. Complexity O(n). */
  public static boolean findSliceOfSum(int[] intArray, int checkSum) {
    int frontIndex = 0;
    int sum = 0;
    for (int backIndex = 0; backIndex < intArray.length; backIndex++) {
      while (frontIndex < intArray.length && sum + intArray[frontIndex] <= checkSum) {
        sum += intArray[frontIndex];
        frontIndex += 1;
      }
      if (sum == checkSum) {
        return true;
      }
      sum -= intArray[backIndex];
    }
    return false;
  }
}
