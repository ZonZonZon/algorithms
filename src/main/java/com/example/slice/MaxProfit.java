package com.example.slice;

public class MaxProfit {

  /**
   * Given a log of stock prices compute the maximum possible earning per one transaction. If only
   * loss is possible - then 0.
   */
  public static int getMaxProfit(int[] intArray) {
    int nextSum = 0;
    int maxSum = 0;
    for (int i = 0; i < intArray.length; i++) {
      int number = intArray[i];
      int nextNumber = i + 1 < intArray.length ? intArray[i + 1] : 0;
      int spread = nextNumber - number;
      nextSum = Math.max(0, nextSum + spread);
      maxSum = Math.max(maxSum, nextSum);
    }
    return maxSum;
  }
}
