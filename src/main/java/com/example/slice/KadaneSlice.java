package com.example.slice;

public class KadaneSlice {

  /** Bidirectional Kadane's algorithm. Complexity O(N). */
  public static int getMaxSumSubArray(int[] intArray) {
    int localMax = 0;
    int globalMax = Integer.MIN_VALUE;
    for (int value : intArray) {
      localMax = Math.max(value, value + localMax);
      if (localMax > globalMax) {
        globalMax = localMax;
      }
    }
    return globalMax;
  }
}
