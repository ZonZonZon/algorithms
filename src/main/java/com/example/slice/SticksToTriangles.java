package com.example.slice;

public class SticksToTriangles {

  /**
   * Counts the number of triangles that can be built from given sticks. One side of a triangle
   * should exceed the sum of two other sides. Uses Caterpillar method. Complexity O(n2).
   */
  public static int countTriangles(int[] stickLengths) {
    int result = 0;
    for (int stickXIndex = 0; stickXIndex < stickLengths.length; stickXIndex++) {
      int stickZIndex = stickXIndex + 2;
      for (int stickYIndex = stickXIndex + 1; stickYIndex < stickLengths.length; stickYIndex++) {
        boolean hasMoreSticks = stickZIndex < stickLengths.length;
        boolean matchesTriangleCondition =
            hasMoreSticks
            && stickLengths[stickXIndex] + stickLengths[stickYIndex] > stickLengths[stickZIndex];

        while (hasMoreSticks && matchesTriangleCondition) {
          stickZIndex += 1;
          break;
        }
        result += stickZIndex - stickYIndex - 1;
      }
    }
    return result;
  }
}
