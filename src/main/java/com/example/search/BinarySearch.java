package com.example.search;

import java.util.Arrays;

public class BinarySearch {

  /**
   * Splits ordered values into two parts. One part is selected by condition to continue the search.
   */
  public static int findNumber(int[] numbers, int expected) {
    int startIndex = 0;
    int endIndex = numbers.length - 1;
    int resultIndex = -1;
    Arrays.sort(numbers);
    while (startIndex <= endIndex) {
      int middleIndex = (startIndex + endIndex) / 2;
      if (numbers[middleIndex] <= expected) {
        startIndex = middleIndex + 1;
        resultIndex = middleIndex;
      } else {
        endIndex = middleIndex - 1;
      }
    }
    return numbers[resultIndex];
  }
}
