package com.example.search;

import java.util.Arrays;

public class MinMaxDivision {

  /**
   * Groups array numbers into blocks. Each number should be grouped. Blocks can be empty. Numbers
   * fall into blocks in the order they are given.
   *
   * @return The smallest block sum of grouping combinations.
   */
  public static int findMinMaxSum(int givenBlocksAmount, int maxValue, int[] numbers) {
    int high = Arrays.stream(numbers).sum();
    // Real max value can be smaller that the given one:
    int low = Arrays.stream(numbers).max().orElseThrow();
    int mid = 0;
    int smallestSum = 0;
    // Minimal max value will be between high and low:
    while (high >= low) {
      // Loop from low to high with binary search division:
      mid = (high + low) / 2;
      int requiredBlocksAmount = countBlocks(mid, numbers);
      if (requiredBlocksAmount > givenBlocksAmount) {
        // The minimal sum is above the middle:
        low = mid + 1;
      } else {
        // The minimal sum is below the middle:
        smallestSum = mid;
        high = mid - 1;
      }
    }
    return smallestSum;
  }

  /** Detects how many blocks are required to group the numbers and follow the max condition. */
  public static int countBlocks(int max, int[] numbers) {
    int current = 0;
    int count = 1;
    for (int number : numbers) {
      if (current + number > max) {
        // This block is overfilled with numbers - switching to the next one:
        current = number;
        count++;
      } else {
        // Continue filling th block with numbers:
        current += number;
      }
    }
    return count;
  }
}
