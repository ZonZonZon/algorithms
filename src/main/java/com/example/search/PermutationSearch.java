package com.example.search;

public class PermutationSearch {

  /** Finds a contiguous sequence in a random numbers array. */
  public static int isPermutation(int[] intArray) {
    int[] orderedArray = new int[intArray.length];
    int flagsSum = 0;
    for (int number : intArray) {
      boolean hasNext = number - 1 >= 0 && number - 1 < intArray.length;
      if (hasNext && orderedArray[number - 1] == 0) {
        // Fill ordered array:
        orderedArray[number - 1] = 1;
        flagsSum++;
      }
      if (flagsSum == intArray.length) {
        // Ordered array is finished:
        return 1;
      }
    }
    // No sequence detected:
    return 0;
  }
}
