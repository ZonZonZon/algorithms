package com.example.counting;

import java.util.stream.IntStream;

public class SwapArrayNumbers {

  /** Swaps array numbers to make array sums equal. Complexity is O(n2) */
  public static int[] getSwapNumbersToEqualizeArraySums1(int[] intArray1, int[] intArray2) {
    int sum1 = IntStream.of(intArray1).sum();
    int sum2 = IntStream.of(intArray2).sum();
    int difference = sum1 - sum2;
    if (difference == 1) {
      // Impossible:
      return new int[0];
    }
    for (int int1 : intArray1) {
      for (int int2 : intArray2) {
        if (int1 - int2 == difference / 2) {
          return new int[]{int1, int2};
        }
      }
    }
    return new int[0];
  }
}
