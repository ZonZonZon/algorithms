package com.example.counting;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class MinMissingInteger {

  /**
   * Finds the smallest positive integer (>0) that is not in the given array. Complexity O(N) or O(N
   * * log(N))
   */
  public static int getMinMissingInteger1(int[] intArray) {
    final int MAX_VALUE = 1000000;
    final int MIN_MISSING_VALUE = 1;
    Arrays.sort(intArray);
    if (intArray[0] > 1) {
      // Min missing value is not in the array:
      return MIN_MISSING_VALUE;
    }
    for (int i = 0; i < intArray.length; i++) {
      int number = intArray[i];
      boolean hasNext = i + 1 < intArray.length;
      int next = hasNext ? intArray[i + 1] : 0;
      int missingValue = number + 1 == 0 ? number + 2 : number + 1;
      boolean hasAGap = missingValue > 0 && next - missingValue > 0;
      if (hasNext && next > 0 && missingValue <= MAX_VALUE && hasAGap) {
        // A gap case:
        return missingValue;
      }
    }
    if (intArray[intArray.length - 1] > 0 && intArray[intArray.length - 1] + 1 <= MAX_VALUE) {
      // All numbers are consecutive:
      return intArray[intArray.length - 1] + 1;
    }
    // All other cases = absolute min positive:
    return 1;
  }

  /**
   * Finds the smallest positive integer (>0) that is not in the given array. Complexity O(N**2);
   */
  public int getMinMissingInteger2(int[] A) {
    Set<Integer> testedSet = new TreeSet<>();
    Set<Integer> perfectSet = new TreeSet<>();

    for(int i=0; i<A.length; i++) {
      testedSet.add(A[i]);   //convert array to set to get rid of duplicates, order int's
      perfectSet.add(i+1);  //create perfect set so can find missing int
    }

    for(Integer current : perfectSet) {
      if(!testedSet.contains(current)) {
        return current;
      }
    }

    if(perfectSet.size() == testedSet.size()) {
      return perfectSet.size() + 1;  //e.g. {1, 2, 3} should return 4
    }

    return 1; //default - e.g. if A array has negative values or just a single positive value like 10
  }
}
