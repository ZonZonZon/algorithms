package com.example.counting;

public class MaxCounters {

  /**
   * Calculate the values of counters after applying all alternating operations: 1) increase counter
   * value by 1 if operation value is within 1 & counters amount (increase the counter by index =
   * operation value - 1); 2) set value of all counters to the current counters maximum if .
   * Solution doesn't update the whole array, but keeps the max value in the variable to be applied
   * when the counter is used.
   *
   * @param countersAmount Amount of counters to update. Initialized to zero by default. Can be in a
   *     range 1..100000.
   * @param intArray Array of numbers as operations. Array size can be in a range 1..100000. Each
   *     value is in the range 1..N+1, where N is amount of counters.
   * @return All counters updated.
   */
  public static int[] updateCounters(int countersAmount, int[] intArray) {
    final int maxOperationTriggeringValue = countersAmount + 1;
    int maxValue = 0;
    int lastValue = 0;
    int counters[] = new int[countersAmount];

    for (int i = 0; i < intArray.length; i++) {
      int operationValue = intArray[i];
      if (operationValue == maxOperationTriggeringValue) {
        lastValue = maxValue;
      } else {
        int counterIndex = operationValue - 1;
        if (counters[counterIndex] < lastValue) {
          counters[counterIndex] = lastValue + 1;
        } else {
          counters[counterIndex]++;
        }
        if (counters[counterIndex] > maxValue) {
          maxValue = counters[counterIndex];
        }
      }
    }
    for (int i = 0; i < countersAmount; i++) {
      if (counters[i] < lastValue) counters[i] = lastValue;
    }
    return counters;
  }
}
