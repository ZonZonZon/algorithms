package com.example.counting;

public class FrogRiverEarliestJumps {

  /**
   * Find the earliest time when a frog can get to the other side of a river by tree leaves. Initial
   * frog position is 0. The frog jumps 1 position at a time. Leaves fall on the water one per
   * second in different places. Leaves don't float and their position is stable. Complexity is
   * O(N).
   *
   * @param lastRiverPosition Last position to jump on the river.
   * @param leavePositions A sequence of seconds (array index) and leave positions (array value).
   * @return The earliest time the frog can get to the last leave at the bank (index value, e.g.
   *     first second is 0).
   */
  public static int getEarliestTime(int lastRiverPosition, int[] leavePositions) {
    int currentPosition = 0;
    int[] path = new int[lastRiverPosition + 1];
    int pathSum = 0;
    for (int second = 0; second < leavePositions.length; second++) {
      int leavePosition = leavePositions[second];
      if (path[leavePosition] == 0) {
        path[leavePosition] = 1;
        pathSum++;
      }
      if (pathSum == lastRiverPosition) {
        return second;
      }
    }
    return -1;
  }
}
