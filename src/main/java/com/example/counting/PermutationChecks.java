package com.example.counting;

public class PermutationChecks {

  /**
   * Check whether array A is a permutation. Array is a permutation if it contains all numbers of a
   * sequence starting with 1, but they can be randomly reordered. It is a permutation if each
   * sequence value is met only once. It is not a permutation if there is only one value, as it is
   * not a sequence. Complexity is O(N) or O(N * log(N)).
   *
   * @param intArray An array of integers.
   * @return 1 if the array is a permutation. 0 otherwise.
   */
  public static int isPermutation(int[] intArray) {
    int[] orderedArray = new int[intArray.length];
    int flagsSum = 0;
    for (int number : intArray) {
      boolean hasNext = number - 1 >= 0 && number - 1 < intArray.length;
      if (hasNext && orderedArray[number - 1] == 0) {
        orderedArray[number - 1] = 1;
        flagsSum++;
      }
      if (flagsSum == intArray.length) {
        return 1;
      }
    }
    return 0;
  }
}
