package com.example.counting;

import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class CharsCounter {

  /**
   * Please codify the input string to return chars in alphabetical order. Each char should be
   * followed by a number of this char occurrences. Skip char counter if zero.
   */
  public static String changeStringToLettersCounter1(String charArray) {
    StringBuilder finalString = new StringBuilder();
    TreeMap<String, Integer> tree = new TreeMap();
    for (char c : charArray.toCharArray()) {
      int occurancesCounter = 0;
      for (int i = 0; i < charArray.length(); i++) {
        if (charArray.charAt(i) == c) {
          occurancesCounter++;
        }
      }
      tree.put(String.valueOf(c), occurancesCounter > 1 ? occurancesCounter : 0);
    }
    // Build a result string:
    Set<Entry<String, Integer>> entrySet = tree.entrySet();
    for (Entry<String, Integer> entry : entrySet) {
      finalString.append(entry.getKey()).append(entry.getValue() == 0 ? "" : entry.getValue());
    }
    return finalString.toString();
  }
}
