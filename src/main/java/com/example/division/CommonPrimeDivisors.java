package com.example.division;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class CommonPrimeDivisors {

  /** Check whether two numbers have the same prime divisors. Complexity O(Z * (max(A) + max(B))).*/
  public static int getCommonPrimeDivisors(int[] intArrayA, int[] intArrayB) {
    Map<Integer, int[]> primeNumbers = new HashMap<>();
    // Precalculate prime numbers for the given numbers:
    IntStream concatenated = IntStream.concat(Arrays.stream(intArrayA), Arrays.stream(intArrayB));
    for (int number : concatenated.toArray()) {
      if (!primeNumbers.containsKey(number)) {
        int[] numberPrimeNumbers = EratosthenesSieve.getPrimeNumbers(number);

        int[] primeDivisors =
            Arrays.stream(numberPrimeNumbers)
                .boxed()
                .filter(integer -> number % integer == 0)
                .mapToInt(integer -> integer)
                .toArray();

        primeNumbers.put(number, primeDivisors);
      }
    }
    // Compare prime number sets:
    int result = 0;
    for (int i = 0; i < intArrayA.length; i++) {
      int[] aPrimeNumbers = primeNumbers.get(intArrayA[i]);
      int[] bPrimeNumbers = primeNumbers.get(intArrayB[i]);
      if (Arrays.equals(aPrimeNumbers, bPrimeNumbers)) {
        result++;
      }
    }
    return result;
  }
}
