package com.example.division;

import java.util.ArrayList;
import java.util.List;

public class CoinFlippers {

  /**
   * Persons flip coins. Amount of persons is equal to the amount of coins. All coins are
   * eagle-sided at the beginning. Complexity O(n log n).
   *
   * @return Resulting coins state after flipping.
   */
  public static int[] getCoinsFlipped(int[] intArray) {
    List<Integer> result = new ArrayList<>();
    // Prime numbers remain on the same side:
    for (int number : intArray) {
      int divisors = CountDivisors.getDivisors(number);
      boolean isFlipped = divisors == 0 || divisors % 2 != 0;
      if (!isFlipped) {
        result.add(number);
      }
    }
    return result.stream().mapToInt(Integer.class::cast).sorted().toArray();
  }
}
