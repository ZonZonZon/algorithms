package com.example.division;

public class BinaryEuclideanAlgorithm {

  /**
   * Find the greatest common divisor. Complexity is O(log n).
   * */
  public static int getGreatestCommonDivisor(int a, int b, int currentGcd) {
    if (a == b) {
      return currentGcd * a;
    } else if ((a % 2 == 0) && (b % 2 == 0)) {
      return getGreatestCommonDivisor(a / 2, b / 2, 2 * currentGcd);
    } else if (a % 2 == 0) {
      return getGreatestCommonDivisor(a / 2, b, currentGcd);
    } else if (b % 2 == 0) {
      return getGreatestCommonDivisor(a, b / 2, currentGcd);
    } else if (a > b) {
      return getGreatestCommonDivisor(a - b, b, currentGcd);
    } else {
      return getGreatestCommonDivisor(a, b - a, currentGcd);
    }
  }

  /**
   * Find the least common multiple. Complexity is O(log n).
   * */
  public static int getLeastCommonMultiple(int a, int b) {
    int lcm = (a * b) / getGreatestCommonDivisor(a, b, 1);
    return lcm;
  }
}
