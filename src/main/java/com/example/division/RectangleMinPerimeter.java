package com.example.division;

public class RectangleMinPerimeter {

  /** Counts the min perimeter for a rectangle of a given area. Complexity O(n). */
  public static int getRectangleMinPerimeter(int area) {
    int minPerimeter = Integer.MAX_VALUE;
    for (int i = 1; i <= Math.sqrt(area); i++) {
      if (area % i != 0) {
        continue;
      }
      int sideA = i;
      int sideB = area / sideA;
      int perimeter = (sideA + sideB) * 2;
      minPerimeter = Math.min(perimeter, minPerimeter);
      if (sideA > sideB) {
        break;
      }
    }
    return minPerimeter;
  }
}
