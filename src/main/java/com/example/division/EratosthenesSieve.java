package com.example.division;

import java.util.ArrayList;
import java.util.List;

public class EratosthenesSieve {

  /**
   * Throws out multiples of the smallest number as they are complex numbers. Complexity O(n log log n).
   *
   * @param maxValue A max value of a consecutive array.
   * @return Prime numbers.
   */
  public static int[] getPrimeNumbers(int maxValue) {
    boolean[] isPrime = new boolean[maxValue + 1];
    for (int i = 0; i <= maxValue; i++) {
      isPrime[i] = true;
    }
    for (int number = 2; number * number <= maxValue; number++) {
      if (isPrime[number]) {
        // Looping over non-complex numbers only:
        for (int nextNumber = number * number; nextNumber <= maxValue; nextNumber += number) {
          // Looping only numbers above the square of the number:
          isPrime[nextNumber] = false;
        }
      }
    }
    List<Integer> numbers = new ArrayList<>();
    for (int number = 2; number <= maxValue; number++) {
      if (isPrime[number]) {
        numbers.add(number);
      }
    }
    return numbers.stream().mapToInt(Integer.class::cast).sorted().toArray();
  }

  /**
   * Keeps multiples of the smallest number as they are complex numbers. Complexity O(n log log n).
   *
   * @param maxValue A max value of a consecutive array.
   * @return Composite numbers.
   */
  public static int[] getCompositeNumbers(int maxValue) {
    boolean[] isPrime = new boolean[maxValue + 1];
    for (int i = 0; i <= maxValue; i++) {
      isPrime[i] = true;
    }
    for (int number = 2; number * number <= maxValue; number++) {
      if (isPrime[number]) {
        // Looping over non-complex numbers only:
        for (int nextNumber = number * number; nextNumber <= maxValue; nextNumber += number) {
          // Looping only numbers above the square of the number:
          isPrime[nextNumber] = false;
        }
      }
    }
    List<Integer> numbers = new ArrayList<>();
    for (int number = 2; number <= maxValue; number++) {
      if (!isPrime[number]) {
        numbers.add(number);
      }
    }
    return numbers.stream().mapToInt(Integer.class::cast).sorted().toArray();
  }
}
