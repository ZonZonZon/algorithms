package com.example.division;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorization {

  /**
   * Finds prime factors the number can be decomposed to. Complexity O(log x).
   *
   * @param number Number to decompose into factors.
   * @return Factors that produce the number when multiplied.
   */
  public static int[] getPrimeFactors(int number) {
    int[] primeNumbers = EratosthenesSieve.getPrimeNumbers(number);
    int remainder = number;
    List<Integer> factors = new ArrayList<>();
    for (int factor : primeNumbers) {
      while (remainder % factor == 0) {
        factors.add(factor);
        remainder /= factor;
      }
    }
    if (remainder != 1) {
      return new int[] {number};
    }
    return factors.stream().mapToInt(integer -> integer).toArray();
  }
}
