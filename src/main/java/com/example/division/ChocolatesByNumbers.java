package com.example.division;

public class ChocolatesByNumbers {

  /**
   * Chocolates are in a circle. When a chocolate is eaten - a wrapper is left instead. Next
   * chocolate to it is some positions away (chocolates and wrappers are skipped). You stop eating
   * when you face an empty wrapper. Count the number of chocolates you will eat.
   */
  public static int getNumberOfChocolatesToEat(int chocolatesCount, int shiftCount) {
    int gcd = BinaryEuclideanAlgorithm.getGreatestCommonDivisor(chocolatesCount, shiftCount, 1);
    return chocolatesCount / gcd;
  }
}
