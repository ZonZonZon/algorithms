package com.example.division;

public class Numismatists {

  /**
   * Counts a common minimum sum from coins of three different denominations. Complexity is O(log
   * n).
   */
  public static int getCommonMinimalSum(int valueA, int valueB, int valueC) {
    int firstPair = BinaryEuclideanAlgorithm.getLeastCommonMultiple(valueA, valueB);
    int secondPair = BinaryEuclideanAlgorithm.getLeastCommonMultiple(valueB, valueC);
    int thirdPair = BinaryEuclideanAlgorithm.getLeastCommonMultiple(valueA, valueC);
    int commonMinimalSum = Math.max(Math.max(firstPair, secondPair), thirdPair);
    return commonMinimalSum;
  }
}
