package com.example.division;

public class CountDivisors {

  /** Getting numbers of divisors starting from 2 (1 is not a prime number). Complexity O( √n). */
  public static int getDivisors(int number) {
    int divisor = 1;
    int divisorsFound = 0;
    while (divisor * divisor < number) {
      if (number % divisor == 0) {
        // Dividable to 1 and itself:
        divisorsFound += 2;
      }
      divisor += 1;
      if (divisor * divisor == number) {
        // Intermediate divisor is found:
        divisorsFound += 1;
      }
    }
    return divisorsFound;
  }
}
