package com.example.division;

public class CountFactors {

  /**
   * Counts the number of factor combinations for N=D*M. The idea is to loop through only a part of
   * combinations - only a square root of the given number. Complexity O(sqrt(N)).
   */
  public static int countFactors(int numberToCheck) {
    int factorCount = 0;
    int sqrt = (int) Math.ceil((Math.sqrt(numberToCheck)));
    // Start from 1 as we want our method to also work when numberToCheck is 0 or 1.
    for (int i = 1; i < sqrt; i++) {
      if (numberToCheck % i == 0) {
        factorCount += 2; //  We found a pair of factors.
      }
    }
    // Check if our number is an exact square.
    if (sqrt * sqrt == numberToCheck) {
      factorCount++;
    }
    return factorCount;
  }
}
