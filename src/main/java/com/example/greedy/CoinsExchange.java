package com.example.greedy;

import java.util.ArrayList;
import java.util.List;

public class CoinsExchange {

  /**
   * Finds the minimum number of coins with which a given amount of money can be paid. Solution is
   * suboptimal for some given denominations, as not all variants are processed. Complexity О(n +
   * m).
   */
  public static int[] getMinCoinsCombination(int[] denominations, int amountToBePaid) {
    List<Integer> result = new ArrayList<>();
    for (int i = denominations.length - 1; i >= 0; i--) {
      while (amountToBePaid >= denominations[i]) {
        amountToBePaid -= denominations[i];
        result.add(denominations[i]);
      }
    }
    return result.stream().mapToInt(integer -> integer).toArray();
  }
}
