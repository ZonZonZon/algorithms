package com.example.greedy;

import java.util.Arrays;

public class Canoeists {

  /**
   * Seats the canoeists in the minimum number of double canoes not exceeding the maximum load.
   * Complexity О(n + m).
   */
  public static int getMinCoinsCombination(int[] weights, int loadLimit) {
    Arrays.sort(weights);
    int canoes = 0;
    int lighterIndex = 0;
    int heavierIndex = weights.length - 1;
    // Each canoe will have the next heaviest and possibly the next lightest:
    while (heavierIndex >= lighterIndex) {
      boolean matchesLoadLimit = weights[heavierIndex] + weights[lighterIndex] <= loadLimit;
      if (matchesLoadLimit) {
        // Current light weight can be paired with current heavy weight:
        lighterIndex += 1;
      }
      // Switch to the next canoe and heavy weight:
      canoes += 1;
      heavierIndex -= 1;
    }
    return canoes;
  }
}
