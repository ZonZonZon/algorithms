package com.example.array;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RotateArray {

  /**
   * Rotate an array. Rotation of the array means that each element is shifted right by one index,
   * and the last element of the array is moved to the first place.
   *
   * @param intArray An Array to rotate.
   * @param rotationsCount Amount of rotations to be applied.
   * @return A array.
   */
  public static int[] rotateArray1(int[] intArray, int rotationsCount) {
    List<Integer> rotatedList = Arrays.stream(intArray).boxed().collect(Collectors.toList());
    for (int rotationIndex = 0; rotationIndex < rotationsCount; rotationIndex++) {
      if (intArray.length > 0) {
        int itemToShift = rotatedList.get(rotatedList.size() - 1);
        rotatedList.remove(rotatedList.size() - 1);
        rotatedList.add(0, itemToShift);
      }
    }
    return rotatedList.stream().mapToInt(Integer.class::cast).toArray();
  }
}
