package com.example.array;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class OddOccurrencesInArray {

  /**
   * Find the value of the unpaired element. Here complexity is O(n) as we iterate only once with
   * XOR rules. In XOR a ^ a = 0 and a ^ 0 = a and a ^ b = delta. Also in XOR a sequence is
   * commutative so a ^ b ^ c ^ d = x and c ^ b ^ a ^ d = x. Thus, the remainder is always an
   * unpaired value.
   *
   * @param intArray A non-empty array A consisting of N integers. The array contains an odd number
   *     of elements, and each element of the array can be paired with * another element that has
   *     the same value, except for one element that is left unpaired.
   * @return An unpaired array value.
   */
  public static int getUnpairedValue1(int[] intArray) {
    int xor = 0;
    for (int i : intArray) {
      xor = xor ^ i;
    }
    return xor;
  }

  /**
   * Find the value of the unpaired element. Here complexity is O(n^2) as there is a loop in a loop.
   *
   * @param intArray A non-empty array A consisting of N integers. The array contains an odd number
   *     of elements, and each element of the array can be paired with * another element that has
   *     the same value, except for one element that is left unpaired.
   * @return An unpaired array value.
   */
  public static int getUnpairedValue2(int[] intArray) {
    List<Integer> intList = Arrays.stream(intArray).boxed().collect(Collectors.toList());
    intList.sort(Comparator.naturalOrder());

    for (int i = 0; i < intList.size(); i = i + 2) {
      Integer currentValue = intList.get(i);
      Integer nextValue = i + 2 <= intList.size() ? intList.get(i + 1) : null;
      if (!currentValue.equals(nextValue)) {
        return currentValue;
      }
    }

    throw new RuntimeException("Unpaired value is not found");
  }

  /**
   * Find the value of the unpaired element. Here complexity is O(n^2) as there is a loop in a loop.
   *
   * @param intArray A non-empty array A consisting of N integers. The array contains an odd number
   *     of elements, and each element of the array can be paired with * another element that has
   *     the same value, except for one element that is left unpaired.
   * @return An unpaired array value.
   */
  public static int getUnpairedValue3(int[] intArray) {
    List<Integer> intList = Arrays.stream(intArray).boxed().collect(Collectors.toList());

    Integer unpairedInteger =
        intList.stream()
            .filter(
                integer ->
                    intList.stream().filter(integerToPair -> integerToPair.equals(integer)).count()
                            % 2
                        > 0)
            .findAny()
            .orElseThrow();

    return unpairedInteger;
  }
}
