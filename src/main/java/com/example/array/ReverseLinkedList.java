package com.example.array;

import java.util.LinkedList;

public class ReverseLinkedList {

  /**
   * Reverses a Linked list.
   *
   * @param linkedList A linked list to reverse.
   * @return A reversed linked list.
   */
  public static LinkedList<Integer> reverseArray1(LinkedList<Integer> linkedList) {
    LinkedList<Integer> relayLinkedList = new LinkedList<>();
    for (int i = 0; i < linkedList.size(); i++) {
      relayLinkedList.add(linkedList.get(linkedList.size() - i - 1));
    }
    return relayLinkedList;
  }
}
