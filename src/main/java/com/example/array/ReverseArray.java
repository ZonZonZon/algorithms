package com.example.array;

import static org.assertj.core.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReverseArray {

  /**
   * Reverses the array. Iterates over everything.
   *
   * @param intArray An Array to reverse.
   * @return A reversed array.
   */
  public static int[] reverseArray1(int[] intArray) {
    List<Object> intList = new ArrayList<>(asList(intArray));
    List<Integer> reversedList = new ArrayList<>();
    intList.forEach(o -> reversedList.add(0, (Integer) o));
    int[] reversedArray = reversedList.stream().mapToInt(Integer::intValue).toArray();
    return reversedArray;
  }

  /**
   * Reverses the array. Iterates over the first half of the array and exchange each number with a
   * symmetric one in the second part.
   *
   * @param intArray An Array to reverse.
   * @return A reversed array.
   */
  public static int[] reverseArray2(int[] intArray) {
    int[] reversedArray = Arrays.copyOf(intArray, intArray.length);
    for (int intArrayIndex = 0; intArrayIndex < reversedArray.length / 2; intArrayIndex++) {
      int currentValue = reversedArray[intArrayIndex];
      int oppositeIndex = reversedArray.length - intArrayIndex - 1;
      int oppositeValue = reversedArray[oppositeIndex];
      reversedArray[intArrayIndex] = oppositeValue;
      reversedArray[oppositeIndex] = currentValue;
    }
    return reversedArray;
  }
}
