package com.example.complexity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FindMissingElementTest {

  @ParameterizedTest
  @CsvSource({
    "0,'2,3,1,5'",
    "1,'0,2,4,5,3,7,6'",
    "2,'100000,99997,99996,99995,99998'",
    "3,''",
    "4,'1'",
    "5,'1,3,2,4'",
    "6,'5,3,2,4'"
  })
  void intArray_ConsecutiveNumbers_FindOneMissing1(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .filter(aString -> aString != null && !aString.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = FindMissingElement.findMissingElement1(intArray);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(4);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 2) {
      assertThat(result).isEqualTo(99999);
    }
    if (testCaseIndex == 3) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 4) {
      assertThat(result).isEqualTo(2);
    }
    if (testCaseIndex == 5) {
      assertThat(result).isEqualTo(5);
    }
    if (testCaseIndex == 6) {
      assertThat(result).isEqualTo(6);
    }
  }
}
