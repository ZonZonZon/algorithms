package com.example.complexity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class NumberTapeCuttingTest {

  @ParameterizedTest
  @CsvSource({
      "0,'3,1,2,4,3'",
      "1,'-1000,3,1,-5,5,5,3,1000'"
  })
  void intArray_NonEmptyCutIntoTwoParts_ReturnsLeftAndRightSumsMinDifference1(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .filter(aString -> aString != null && !aString.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = NumbersTapeCutting.leftAndRightSumsMinDifference(intArray);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(1988);
    }
  }
}
