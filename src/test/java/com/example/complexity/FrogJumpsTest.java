package com.example.complexity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FrogJumpsTest {

  @ParameterizedTest
  @CsvSource({
    "0,10,85,30",
    "1,500000000,1000000000,12345",
    "2,1000000000,1000000000,1000000000",
    "3,1,1000000000,1000000000",
    "4,1,11,10"
  })
  void frog_JumpsByRoute_MinAmountOfJumpsIsDefined1(
      int testCaseIndex, int startPositionX, int finishPositionY, int jumpLengthD) {

    int result = FrogJumps.countMinFrogJumps(startPositionX, finishPositionY, jumpLengthD);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(3);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(40503);
    }
    if (testCaseIndex == 2) {
      assertThat(result).isZero();
    }
    if (testCaseIndex == 3) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 4) {
      assertThat(result).isEqualTo(1);
    }
  }
}
