package com.example.stacks_queues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import com.example.sorting.HeapSort;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class GroceryStoreTest {

  @ParameterizedTest
  @CsvSource({
      "'0,0,0,1,1,0,1,1',8",
      "'1,0,0,0,0,0,0',1"})
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, String expectedArguments) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int expected = Integer.parseInt(expectedArguments);
    int result = GroceryStore.getMinOperations(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
