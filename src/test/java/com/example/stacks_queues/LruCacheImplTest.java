package com.example.stacks_queues;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class LruCacheImplTest {

  private static LruCache lruCache;

  @BeforeAll
  public static void init(){
    lruCache = new LruCacheImpl(3);
  }

  @Test
  void cachedNumber_NumberIsGot_BecomesTheFirst(){
    lruCache.put(1,1); // cache is {1=1}
    lruCache.put(2,2); // cache is {1=1, 2=2}
    assertThat(lruCache.get(1)).isEqualTo(1); // return 1, cache is {2=2, 1=1}
    lruCache.put(3, 3);   // cache is {2=2, 1=1, 3=3}
    lruCache.put(4, 4);   // LRU key was 2, evicts key 2, cache is {1=1, 3=3, 4=4}
    assertThat(lruCache.get(2)).isNull(); // return null (not found)
    lruCache.put(2, 2);   // LRU key was 1, evicts key 1, cache is {3=3, 4=4, 2=2}
    assertThat(lruCache.get(4)).isEqualTo(4); // return 4, cache is {3=3, 2=2, 4=4}
    assertThat(lruCache.get(1)).isNull(); // return null (not found)
    assertThat(lruCache.get(3)).isEqualTo(3); // return 3, cache is {2=2, 4=4, 3=3}
  }

}