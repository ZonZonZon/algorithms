package com.example.stacks_queues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class AliveFishTest {

  @ParameterizedTest
  @CsvSource({
      "'1','0',1",
      "'4,3,2,1,5','0,1,0,0,0',2",
      "'4,3,2,1,5','0,0,0,0,0',5",
      "'4,3,2,1,5','1,1,1,1,1',5",
      "'4,3,2,1,5','0,1,1,0,0',2",
      "'0,1000000000,999999999,1,5','0,1,0,0,0',2"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, String intArrayArguments2, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] intArray2 =
        asList(intArrayArguments2.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = AliveFish.countAliveFish(intArray, intArray2);
    assertThat(result).isEqualTo(expected);
  }
}
