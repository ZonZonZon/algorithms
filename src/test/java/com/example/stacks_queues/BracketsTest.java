package com.example.stacks_queues;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class BracketsTest {

  @ParameterizedTest
  @CsvSource({
    "'{[()()]}',1",
    "'([)()]',0",
    "'',1",
    "'()',1",
    "'[{}()]',1",
    "'[({})]',1",
    "'([{})]',0",
    "'(()}',0",
    "'(({}))',1",
    "'((((((',0",
    "'({{({}[]{})}}[]{})',1"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, String expectedArguments) {

    String given = intArrayArguments;
    int expected = Integer.parseInt(expectedArguments);
    int result = Brackets.isProperlyNested2(given);
    assertThat(result).isEqualTo(expected);
  }
}
