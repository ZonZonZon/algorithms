package com.example.counting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FrogRiverEarliestJumpsTest {

  @ParameterizedTest
  @CsvSource({"5,'1,3,1,4,2,3,5,4',6", "1,'1',0", "2,'2,1',1", "2,'1,2',1", "100000,'1,100000',-1"})
  void counters1(int lastRiverPosition, String arguments, int expected) {

    int[] intArray =
        asList(arguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = FrogRiverEarliestJumps.getEarliestTime(lastRiverPosition, intArray);
    assertThat(result).isEqualTo(expected);
  }
}
