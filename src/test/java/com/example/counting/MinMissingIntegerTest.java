package com.example.counting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MinMissingIntegerTest {

  @ParameterizedTest
  @CsvSource({
    "0,'1,3,6,4,1,2'",
    "1,'1,2,3'",
    "2,'-1,-3'",
    "3,'-1000000,1000000'",
    "4,'1'",
    "5,'-1'",
    "6,'-1234,1435,-345,1,345,-6'",
    "7,'-1,1,2,3'",
    "8,'1,3,6,4,1,2,5'",
    "9,'-1,-3,2'",
    "10,'-1,-3,1'",
    "11,'0'",
    "12,'-1000000'",
    "13,'-1000000,1'",
    "14,'999999,999998,1000000'",
      "15,'1,3,999999,999998,1000000'",
      "16,'2'"
  })
  void number_AsBinary_MaxGapIsCalculated1(int testCaseIndex, String arguments) {
    int[] intArray =
        asList(arguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MinMissingInteger.getMinMissingInteger1(intArray);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(5);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(4);
    }
    if (testCaseIndex == 2) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 3) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 4) {
      assertThat(result).isEqualTo(2);
    }
    if (testCaseIndex == 5) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 6) {
      assertThat(result).isEqualTo(2);
    }
    if (testCaseIndex == 7) {
      assertThat(result).isEqualTo(4);
    }
    if (testCaseIndex == 8) {
      assertThat(result).isEqualTo(7);
    }
    if (testCaseIndex == 9) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 10) {
      assertThat(result).isEqualTo(2);
    }
    if (testCaseIndex == 11) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 12) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 13) {
      assertThat(result).isEqualTo(2);
    }
    if (testCaseIndex == 14) {
      assertThat(result).isEqualTo(1);
    }
    if (testCaseIndex == 15) {
      assertThat(result).isEqualTo(2);
    }
    if (testCaseIndex == 16) {
      assertThat(result).isEqualTo(1);
    }
  }
}
