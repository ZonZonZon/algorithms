package com.example.counting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MaxCountersTest {

  @ParameterizedTest
  @CsvSource({"0,5,'3,4,4,6,1,4,4'","1,3,'2,1,2'","2,3,'2,1,2,2,1,2'","3,2,'2,1,3,2'","4,2,'2,1,3,2,3,1,2'","5,1,'2,1,2'"})
  void counters1(int testCaseIndex, int countersAmount, String intArguments) {

    int[] intArray =
      asList(intArguments.split(",")).stream()
          .mapToInt(intObject -> Integer.parseInt((String) intObject))
          .toArray();

    int[] result = MaxCounters.updateCounters(countersAmount, intArray);
    if (testCaseIndex == 0) {
      assertThat(result[0]).isEqualTo(3);
      assertThat(result[1]).isEqualTo(2);
      assertThat(result[2]).isEqualTo(2);
      assertThat(result[3]).isEqualTo(4);
      assertThat(result[4]).isEqualTo(2);
    }
    if (testCaseIndex == 1) {
      assertThat(result[0]).isEqualTo(1);
      assertThat(result[1]).isEqualTo(2);
      assertThat(result[2]).isEqualTo(0);
    }
    if (testCaseIndex == 2) {
      assertThat(result[0]).isEqualTo(2);
      assertThat(result[1]).isEqualTo(4);
      assertThat(result[2]).isEqualTo(0);
    }
    if (testCaseIndex == 3) {
      assertThat(result[0]).isEqualTo(1);
      assertThat(result[1]).isEqualTo(2);
    }
    if (testCaseIndex == 4) {
      assertThat(result[0]).isEqualTo(3);
      assertThat(result[1]).isEqualTo(3);
    }
    if (testCaseIndex == 5) {
      assertThat(result[0]).isEqualTo(1);
    }
  }
}
