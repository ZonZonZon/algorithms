package com.example.counting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PermutationCheckTest {

  @ParameterizedTest
  @CsvSource({
    "'4,1,3,2',1",
    "'4,1,3',0",
    "'1,2,3,4',1",
    "'4,3,2,1',1",
    "'4,3,2',0",
    "'2,3,4',0",
    "'2',0",
    "'1',1",
    "'999999999',0",
    "'1000000000',0",
    "'999999999,999999998,999999997',0",
    "'999999998,999999999,1000000000',0"
  })
  void counters1(String arguments, int expected) {

    int[] intArray =
        asList(arguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = PermutationChecks.isPermutation(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
