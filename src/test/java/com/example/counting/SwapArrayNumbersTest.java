package com.example.counting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class SwapArrayNumbersTest {

  @ParameterizedTest
  @CsvSource({"0,'1,33,2,5,5','2,33,1,4,4'"})
  void twoArrays_NumbersAreSwapped_SumsAreEqual1(int testCaseIndex, String intArguments1, String intArguments2) {
    int[] intArray1 =
      asList(intArguments1.split(",")).stream()
          .mapToInt(intObject -> Integer.parseInt((String) intObject))
          .toArray();

    int[] intArray2 =
      asList(intArguments2.split(",")).stream()
          .mapToInt(intObject -> Integer.parseInt((String) intObject))
          .toArray();

    int[] result = SwapArrayNumbers.getSwapNumbersToEqualizeArraySums1(intArray1, intArray2);
    if (testCaseIndex == 0) {
      assertThat(result[0]).isEqualTo(2);
      assertThat(result[1]).isEqualTo(1);
    }
  }
}
