package com.example.counting;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.counting.CharsCounter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CharsCounterTest {

  @ParameterizedTest
  @CsvSource({"0,'ahabcjahbdcjbjj'", "1,'lkjsflksjfsdlkfj'", "2,'f'", "3,''"})
  void number_AsBinary_MaxGapIsCalculated1(int testCaseIndex, String charArguments) {
    String result = CharsCounter.changeStringToLettersCounter1(charArguments);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo("a3b3c2dh2j4");
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo("df3j3k3l3s3");
    }
    if (testCaseIndex == 2) {
      assertThat(result).isEqualTo("f");
    }
    if (testCaseIndex == 3) {
      assertThat(result).isEmpty();
    }
  }
}
