package com.example.search;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import com.example.sum.FibonacciFrog;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class BinarySearchTest {

  @ParameterizedTest
  @CsvSource({
      "'19,31,24,15,53,15,59,12,60',31"
  })
  void aLimitingNumber_FibonacciSequence_IsReturned(String givenArguments, int expected) {

    int[] given =
        asList(givenArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = BinarySearch.findNumber(given, expected);
    assertThat(result).isEqualTo(expected);
  }
}
