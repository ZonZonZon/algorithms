package com.example.search;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MinMaxDivisionTest {

  @ParameterizedTest
  @CsvSource({
      "'2,1,5,1,2,2,2',5,3,6",
      "'2,1,5,1,2,2,2',0,100000,5",
      "'2,1,5,1,2,2,2',10000,100000,5"
  })
  void aLimitingNumber_FibonacciSequence_IsReturned(String givenArguments, int maxValue, int blockAmount, int expected) {

    int[] given =
        asList(givenArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MinMaxDivision.findMinMaxSum(blockAmount, maxValue, given);
    assertThat(result).isEqualTo(expected);
  }
}
