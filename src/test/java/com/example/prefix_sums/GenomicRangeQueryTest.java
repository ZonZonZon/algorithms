package com.example.prefix_sums;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class GenomicRangeQueryTest {

  @ParameterizedTest
  @CsvSource({
      "CAGCCTA,'2,5,0','4,5,6','2,4,1'",
      "TC,'0','1','2'",
      "A,'0','0','1'"
  })
  void integers_DividedByNumber_DividableAreCounted(
      String dnaSequence, String pQueryArguments, String qQueryArguments, String expectedArguments) {

    int[] pQueryArray =
        asList(pQueryArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] qQueryArray =
        asList(qQueryArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] expectedArray =
        asList(expectedArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = GenomicRangeQuery.getDnaMinimalImpacts(dnaSequence, pQueryArray, qQueryArray);
    assertThat(result).isEqualTo(expectedArray);
  }
}
