package com.example.prefix_sums;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PassingCarsTest {

  @ParameterizedTest
  @CsvSource({
      "'0,1,0,1,1',5",
      "'1,0,1,0,0',1",
      "'1,1,0,0,1,1,0,0,0,1,1',14",
      "'0',0",
      "'1',0",
      "'0,1',1",
      "'1,0',0",
      "'0,1,0',1",
      "'1,0,1',1",
      "'1,0,1,0',1",
      "'0,1,0,1',3"
  })
  void cars_MovingEastAndWestOneDirection_PairsAreCounted(String arguments, int expected) {

    int[] intArray =
        asList(arguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = PassingCars.countOneDirectionPassingByPairs(intArray);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
      "'0,1,0,1,1',5",
      "'1,0,1,0,0',5",
      "'1,1,0,0,1,1,0,0,0,1,1',16",
      "'0',0",
      "'1',0",
      "'0,1',1",
      "'1,0',1",
      "'0,1,0',1",
      "'1,0,1',1",
      "'1,0,1,0',3",
      "'0,1,0,1',3"
  })
  void cars_MovingAnyOppositeDirection_AllPairsAreCounted(String arguments, int expected) {

    int[] intArray =
        asList(arguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = PassingCars.countAllPassingByPairs(intArray);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void cars_OverABillion_ANegativeIsReturned() {

    int[] intArray = new int[1000000000];

    for (int i = 0; i < intArray.length; i++) {
      intArray[i] = i > 0 ? intArray[i - 1] + intArray[i] : intArray[i];
    }

    int result = PassingCars.countAllPassingByPairs(intArray);
    assertThat(result).isEqualTo(-1);

    int result2 = PassingCars.countOneDirectionPassingByPairs(intArray);
    assertThat(result2).isEqualTo(-1);
  }
}
