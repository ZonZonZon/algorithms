package com.example.prefix_sums;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CountDivTest {

  @ParameterizedTest
  @CsvSource({
      "6,11,2,3",
      "0,0,11,1",
      "0,2000000000,1,2000000000",
      "0,2000000000,2000000000,1",
      "1999999999,2000000000,1999999999,1"
  })
  void integers_DividedByNumber_DividableAreCounted(int rangeStart, int rangeEnd, int divider, int expected) {
    long result = CountDiv.countDivisibleIntegers(rangeStart, rangeEnd, divider);
    assertThat(result).isEqualTo(expected);
  }
}
