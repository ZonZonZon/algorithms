package com.example.prefix_sums;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MushroomPickerTest {

  @ParameterizedTest
  @CsvSource({
      "4,6,'2,3,7,5,1,3,9',25",
      "2,2,'9,9,1,1,1,1,1',10"
  })
  void counters1(int pickerPosition, int maxPickerMoves, String arguments, int expected) {

    int[] intArray =
        asList(arguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MushroomPicker.collectMushrooms(pickerPosition, maxPickerMoves, intArray);
    assertThat(result).isEqualTo(expected);
  }
}
