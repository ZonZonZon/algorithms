package com.example.prefix_sums;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MinAverageTwoSliceTest {

  @ParameterizedTest
  @CsvSource({
    "'4,2,2,5,1,5,8',1",
    "'4,2,2,5,1,3,8',1",
    "'4,2,2,-10000,1,10000,8',3",
    "'-10000,10000,10000,-100000,0,-1,1',3",
    "'0,0',0",
    "'10000,-10000',0",
  })
  void integersArray_SlicesAveragesCompared_IndexOfMinAverageIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MinAverageTwoSlice.findMinAverageSliceOfTwo(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
