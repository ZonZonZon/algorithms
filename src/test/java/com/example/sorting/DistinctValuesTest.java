package com.example.sorting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DistinctValuesTest {

  @ParameterizedTest
  @CsvSource({"'2,1,1,2,3,1',3", "'-1000000,-1,1,1000000,2,3,1',6", "'',0"})
  void integersArray_DistinctNumbersFound_AreCounted(String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(o -> !o.equals(""))
            .mapToInt(
                intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = DistinctValues.countDistinct(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
