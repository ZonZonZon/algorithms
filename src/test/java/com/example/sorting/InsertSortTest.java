package com.example.sorting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class InsertSortTest {

  @ParameterizedTest
  @CsvSource({
      "'4,2,5,1,8','1,2,4,5,8'",
      "'4,2,2,5,1,5,8','1,2,2,4,5,5,8'"})
  void integersArray_MinElementIsSwappedAndExcluded_TheRestIsSortedAscending(
      String intArrayArguments, String expectedArguments) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] expectedArray =
        asList(expectedArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = InsertSort.sortAscending(intArray);
    assertThat(result).isEqualTo(expectedArray);
  }
}
