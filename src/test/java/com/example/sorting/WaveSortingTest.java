package com.example.sorting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class WaveSortingTest {

  @ParameterizedTest
  @CsvSource({
      "0,'1,2,3,4,-5'",
      "1,'1,5,11,43,1,2,13,7,24,18'"
  })
  void array_OfIntegers_IsReversed1(int testCaseIndex, String intArguments) {
    Integer[] intArray = new Integer[] {};

    try {
      intArray =
          asList(intArguments.split(",")).stream()
              .mapToInt(intObject -> Integer.parseInt((String) intObject))
              .boxed()
              .toArray(Integer[]::new);
    } catch (Exception ignore) {
    }

    Integer[] result = WaveSorting.sortAsWave1(intArray);
    assertAny(testCaseIndex, result);
  }

  private void assertAny(int testCaseIndex, Integer[] result) {
    if (testCaseIndex == 0) {
      assertThat(result[0]).isEqualTo(4);
      assertThat(result[1]).isEqualTo(-5);
      assertThat(result[2]).isEqualTo(3);
      assertThat(result[3]).isEqualTo(1);
      assertThat(result[4]).isEqualTo(2);
    }
    if (testCaseIndex == 1) {
      assertThat(result[0]).isEqualTo(43);
      assertThat(result[1]).isEqualTo(1);
      assertThat(result[2]).isEqualTo(24);
      assertThat(result[3]).isEqualTo(1);
      assertThat(result[4]).isEqualTo(18);
      assertThat(result[5]).isEqualTo(2);
      assertThat(result[6]).isEqualTo(13);
      assertThat(result[7]).isEqualTo(5);
      assertThat(result[8]).isEqualTo(11);
      assertThat(result[9]).isEqualTo(7);
    }
  }
}
