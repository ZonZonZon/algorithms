package com.example.node;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import com.example.node.ReverseLinkedNodes.Node;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ReverseLinkedNodesTest {

  @ParameterizedTest
  @CsvSource({"0,'1,2,3,4'", "1,'4,3,2,1'"})
  void linedNodes_OfIntegers_AreReversed1(int testCaseIndex, String intArguments) {
    List<Integer> intList =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .boxed()
            .collect(Collectors.toList());

    Node given = null;
    Node currentNode = null;

    for (int i = 0; i < intList.size(); i++) {
      if (given == null) {
        given = new Node(intList.get(i), null);
        currentNode = given;
      } else if (i + 1 <= intList.size()) {
        Node nextNode = new Node(intList.get(i), null);
        currentNode.setNext(nextNode);
        currentNode = nextNode;
      }
    }

    Node reversed = Node.reverse(given);
    if (testCaseIndex == 0) {
      for (int i = 0; i < 4; i++) {
        assertThat(reversed.getValue()).isEqualTo(4 - i);
        reversed = reversed.getNext();
      }
    }
    if (testCaseIndex == 1) {
      for (int i = 0; i < 4; i++) {
        assertThat(reversed.getValue()).isEqualTo(1 + i);
        reversed = reversed.getNext();
      }
    }
  }
}
