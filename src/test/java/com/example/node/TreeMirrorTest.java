package com.example.node;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.node.TreeMirror.TreeNode;
import org.junit.jupiter.api.Test;

class TreeMirrorTest {

  /**
   * Convert a tree of
   *      5
   *     / \
   *    4   6
   *   /   / \
   *  3   2   7
   *
   * into
   *
   *      5
   *     / \
   *   6    4
   *  / \    \
   * 7   2    3
   */
  @Test
  void integers_EachOneNeighboursSummed_EqualToRequestedIsReturned() {
    TreeNode node3 = new TreeNode(3, null, null);
    TreeNode node2 = new TreeNode(2, null, null);
    TreeNode node7 = new TreeNode(7, null, null);
    TreeNode node4 = new TreeNode(4, node3, null);
    TreeNode node6 = new TreeNode(6, node2, node7);
    TreeNode node5 = new TreeNode(5, node4, node6);

    TreeNode result = TreeMirror.mirrorDigitsTree(node5);
    assertThat(result.val).isEqualTo(5);
    assertThat(result.left.val).isEqualTo(6);
    assertThat(result.right.val).isEqualTo(4);
    assertThat(result.left.left.val).isEqualTo(7);
    assertThat(result.left.right.val).isEqualTo(2);
    assertThat(result.right.right.val).isEqualTo(3);
  }
}
