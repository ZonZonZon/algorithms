package com.example.node;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.node.BranchSum.TreeNode;
import org.junit.jupiter.api.Test;

class BranchSumTest {

  /**
   * Given a tree of
   *      5
   *     / \
   *    4   6
   *   /   / \
   *  3   2   7
   */
  @Test
  void integers_EachOneNeighboursSummed_EqualToRequestedIsReturned() {
    TreeNode node3 = new TreeNode(3, null, null);
    TreeNode node2 = new TreeNode(2, null, null);
    TreeNode node7 = new TreeNode(7, null, null);
    TreeNode node4 = new TreeNode(4, node3, null);
    TreeNode node6 = new TreeNode(6, node2, node7);
    TreeNode node5 = new TreeNode(5, node4, node6);

    boolean result = BranchSum.hasSum(node5, 13);
    assertThat(result).isTrue();

    result = BranchSum.hasSum(node5, 12);
    assertThat(result).isTrue();

    result = BranchSum.hasSum(node5, 9);
    assertThat(result).isFalse();

    result = BranchSum.hasSum(node5, 100);
    assertThat(result).isFalse();
  }
}
