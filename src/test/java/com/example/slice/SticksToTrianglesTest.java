package com.example.slice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class SticksToTrianglesTest {

  @ParameterizedTest
  @CsvSource({
    "'6,2,7,4,1,3,6',12"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = SticksToTriangles.countTriangles(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
