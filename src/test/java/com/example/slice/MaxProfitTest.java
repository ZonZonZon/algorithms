package com.example.slice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MaxProfitTest {

  @ParameterizedTest
  @CsvSource({
      "'23171,21011,21123,21366,21013,21367',356",
      "'0,200000',200000",
      "'23171,23170,23169',0",
      "'',0",
      "'0',0"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MaxProfit.getMaxProfit(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
