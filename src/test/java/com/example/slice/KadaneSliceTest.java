package com.example.slice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class KadaneSliceTest {

  @ParameterizedTest
  @CsvSource({
      "'3,2,6,-1,4,5,-1,2',20",
      "'0,10,-5,-2,0',10"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = KadaneSlice.getMaxSumSubArray(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
