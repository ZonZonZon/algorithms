package com.example.slice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MaxPositiveSliceTest {

  @ParameterizedTest
  @CsvSource({
    "'1,2,3,4,5',15",
    "'',0",
    "'0',0",
      "'-1,2,3,-2',5",
      "'2,3,-20,1,8',9",
      "'-2,-3,-20,-1,-8',0",
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MaxSlice.getMaxSlice(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
