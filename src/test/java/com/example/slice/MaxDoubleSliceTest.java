package com.example.slice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MaxDoubleSliceTest {

  @ParameterizedTest
  @CsvSource({
      "'3,2,6,-1,4,5,-1,2',17",
      "'0,10,-5,-2,0',10",
      "'-10000,0,10000,-1,0,0,24,15,6,4,5,-1,2',10054",
      "'0',-10000",
      "'1,2',-10000",
      "'-10000',-10000",
      "'10000',-10000"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = MaxDoubleSlice.getDoubleSliceMaxSum(intArray);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
    "'3,2,6,-1,4,5,-1,2',0,3,6,17",
    "'3,2,6,-1,4,5,-1,2',0,3,7,16",
    "'3,2,6,-1,4,5,-1,2',3,4,5,0",
  })
  void indexesTriple_SummedByRules_SumIsReturned(
      String intArrayArguments, int xIndex, int yIndex, int zIndex, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    // Count accumulating sums:
    int[] sums = new int[intArray.length];
    for (int i = 0; i < intArray.length; i++) {
      if (i == 0) {
        sums[i] = intArray[i];
        continue;
      }
      sums[i] = intArray[i] + sums[i-1];
    }

    int result = MaxDoubleSlice.countSum(intArray, sums, xIndex, yIndex, zIndex);
    assertThat(result).isEqualTo(expected);
  }
}
