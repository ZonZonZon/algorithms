package com.example.leader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class EquiLeaderTest {

  @ParameterizedTest
  @CsvSource({
      "'4,3,4,4,4,2',2",
      "'4',0",
      "'4,4',1",
      "'0,4,0,0',2",
      "'-1000000000,-1000000000,1000000000',0"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = EquiLeader.getEquiLeader(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
