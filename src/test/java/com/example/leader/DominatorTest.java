package com.example.leader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import java.util.Objects;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DominatorTest {

  @ParameterizedTest
  @CsvSource({
      "'1,2,3,4,5',-1,-1,-1,-1,-1",
      "'3,4,5,3,2,3,1,3,3',0,3,5,7,8",
      "'3,4,3,2,3,1,3,3',0,2,4,6,7",
      "'',-1,-1,-1,-1,-1",
      "'4',0,0,0,0,0",
      "'3,4',-1,-1,-1,-1,-1",
      "'1,2,1',0,0,0,0,2",
      "'2,1,1,3',-1,-1,-1,-1,-1"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments,
      String expectedArguments1,
      String expectedArguments2,
      String expectedArguments3,
      String expectedArguments4,
      String expectedArguments5) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    Stream<Integer> expected =
        Stream.of(
            Integer.valueOf(expectedArguments1),
            Integer.valueOf(expectedArguments2),
            Integer.valueOf(expectedArguments3),
            Integer.valueOf(expectedArguments4),
            Integer.valueOf(expectedArguments5));

    int result = Dominator.getMostFrequentIndex(intArray);
    assertThat(expected).contains(result);
  }
}
