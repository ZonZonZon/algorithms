package com.example.iteration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class NeighboursSumTest {

  @ParameterizedTest
  @CsvSource({
      "'1,8,6,10,22,3,44',35,true",
      "'1,8,6,10,22,3,44',13,false"
  })
  void integers_EachOneNeighboursSummed_EqualToRequestedIsReturned(String intArrayArguments, int target, boolean expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    boolean result = NeighboursSum.hasNeighboursSum(intArray, target);
    assertThat(result).isEqualTo(expected);
  }
}
