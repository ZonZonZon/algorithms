package com.example.iteration;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class BinaryGapTest {

  @ParameterizedTest
  @CsvSource({"0,1", "1,2", "2,147", "3,483", "4,647", "5,561892", "6,74901729", "7,1376796946"})
  void number_AsBinary_MaxGapIsCalculated1(int testCaseIndex, int n) {
    int maxBinaryIntervals = BinaryGap.getMaxBinaryGap1(n);
    if (testCaseIndex == 0) {
      assertThat(maxBinaryIntervals).isZero();
    }
    if (testCaseIndex == 1) {
      assertThat(maxBinaryIntervals).isZero();
    }
    if (testCaseIndex == 2) {
      assertThat(maxBinaryIntervals).isEqualTo(2);
    }
    if (testCaseIndex == 3) {
      assertThat(maxBinaryIntervals).isEqualTo(3);
    }
    if (testCaseIndex == 4) {
      assertThat(maxBinaryIntervals).isEqualTo(4);
    }
    if (testCaseIndex == 5) {
      assertThat(maxBinaryIntervals).isEqualTo(3);
    }
    if (testCaseIndex == 6) {
      assertThat(maxBinaryIntervals).isEqualTo(4);
    }
    if (testCaseIndex == 7) {
      assertThat(maxBinaryIntervals).isEqualTo(5);
    }
  }

  @ParameterizedTest
  @CsvSource({"0,1", "1,2", "2,147", "3,483", "4,647", "5,561892", "6,74901729", "7,1376796946"})
  void number_AsBinary_MaxGapIsCalculated2(int testCaseIndex, int n) {
    int maxBinaryIntervals = BinaryGap.getMaxBinaryGap1(n);
    if (testCaseIndex == 0) {
      assertThat(maxBinaryIntervals).isZero();
    }
    if (testCaseIndex == 1) {
      assertThat(maxBinaryIntervals).isZero();
    }
    if (testCaseIndex == 2) {
      assertThat(maxBinaryIntervals).isEqualTo(2);
    }
    if (testCaseIndex == 3) {
      assertThat(maxBinaryIntervals).isEqualTo(3);
    }
    if (testCaseIndex == 4) {
      assertThat(maxBinaryIntervals).isEqualTo(4);
    }
    if (testCaseIndex == 5) {
      assertThat(maxBinaryIntervals).isEqualTo(3);
    }
    if (testCaseIndex == 6) {
      assertThat(maxBinaryIntervals).isEqualTo(4);
    }
    if (testCaseIndex == 7) {
      assertThat(maxBinaryIntervals).isEqualTo(5);
    }
  }
}
