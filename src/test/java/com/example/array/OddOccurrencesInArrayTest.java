package com.example.array;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class OddOccurrencesInArrayTest {

  @ParameterizedTest
  @CsvSource({
      "0,'1,1000000,1000000,1,2,3,3,2,1,10,10,1,9'",
      "1,'1000000000,88,77,88,77,1000000000,12'",
      "2,'22,55,1000000000,22,55'"
  })
  void array_OfIntegers_IsReversed1(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = OddOccurrencesInArray.getUnpairedValue1(intArray);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(9);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(12);
    }
    if (testCaseIndex == 2) {
      assertThat(result).isEqualTo(1000000000);
    }
  }

  @ParameterizedTest
  @CsvSource({
      "0,'1,1000000,1000000,1,2,3,3,2,1,10,10,1,9'",
      "1,'1000000000,88,77,88,77,1000000000,12'",
      "2,'22,55,1000000000,22,55'"
  })
  void array_OfIntegers_IsReversed2(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = OddOccurrencesInArray.getUnpairedValue2(intArray);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(9);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(12);
    }
    if (testCaseIndex == 2) {
      assertThat(result).isEqualTo(1000000000);
    }
  }

  @ParameterizedTest
  @CsvSource({
      "0,'1,1000000,1000000,1,2,3,3,2,1,10,10,1,9'",
      "1,'1000000000,88,77,88,77,1000000000,12'",
      "2,'22,55,1000000000,22,55'"
  })
  void array_OfIntegers_IsReversed3(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = OddOccurrencesInArray.getUnpairedValue3(intArray);
    if (testCaseIndex == 0) {
      assertThat(result).isEqualTo(9);
    }
    if (testCaseIndex == 1) {
      assertThat(result).isEqualTo(12);
    }
    if (testCaseIndex == 2) {
      assertThat(result).isEqualTo(1000000000);
    }
  }
}
