package com.example.array;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import java.util.LinkedList;
import java.util.stream.Collectors;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ReverseLinkedListTest {

  @ParameterizedTest
  @CsvSource({"0,'1,2,3,4'", "1,'4,3,2,1'"})
  void linkedList_OfIntegers_IsReversed1(int testCaseIndex, String intArguments) {
    LinkedList<Integer> given =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .boxed()
            .collect(Collectors.toCollection(LinkedList::new));

    LinkedList<Integer> result = ReverseLinkedList.reverseArray1(given);
    if (testCaseIndex == 0) {
      assertThat(result.get(0)).isEqualTo(4);
      assertThat(result.get(1)).isEqualTo(3);
      assertThat(result.get(2)).isEqualTo(2);
      assertThat(result.get(3)).isEqualTo(1);
    }
    if (testCaseIndex == 1) {
      assertThat(result.get(0)).isEqualTo(1);
      assertThat(result.get(1)).isEqualTo(2);
      assertThat(result.get(2)).isEqualTo(3);
      assertThat(result.get(3)).isEqualTo(4);
    }
  }
}
