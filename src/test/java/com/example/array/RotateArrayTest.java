package com.example.array;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RotateArrayTest {

  @ParameterizedTest
  @CsvSource({
    "0,'1,2,3,4,-5',3",
    "1,'22,55,-444,19,0',11",
    "2,'22,55,444,-19,0',0",
    "3,'-22,55,444,19,0',100",
    "4,'',10"
  })
  void array_OfIntegers_IsReversed1(int testCaseIndex, String intArguments, int rotationsCount) {
    int[] intArray = new int[] {};

    try {
      intArray =
          asList(intArguments.split(",")).stream()
              .mapToInt(intObject -> Integer.parseInt((String) intObject))
              .toArray();
    } catch (Exception ignore) {
    }

    int[] result = RotateArray.rotateArray1(intArray, rotationsCount);
    if (testCaseIndex == 0) {
      assertThat(result[0]).isEqualTo(3);
      assertThat(result[1]).isEqualTo(4);
      assertThat(result[2]).isEqualTo(-5);
      assertThat(result[3]).isEqualTo(1);
      assertThat(result[4]).isEqualTo(2);
    }
    if (testCaseIndex == 1) {
      assertThat(result[0]).isZero();
      assertThat(result[1]).isEqualTo(22);
      assertThat(result[2]).isEqualTo(55);
      assertThat(result[3]).isEqualTo(-444);
      assertThat(result[4]).isEqualTo(19);
    }
    if (testCaseIndex == 2) {
      assertThat(result[0]).isEqualTo(22);
      assertThat(result[1]).isEqualTo(55);
      assertThat(result[2]).isEqualTo(444);
      assertThat(result[3]).isEqualTo(-19);
      assertThat(result[4]).isZero();
    }
    if (testCaseIndex == 3) {
      assertThat(result[0]).isEqualTo(-22);
      assertThat(result[1]).isEqualTo(55);
      assertThat(result[2]).isEqualTo(444);
      assertThat(result[3]).isEqualTo(19);
      assertThat(result[4]).isZero();
    }
    if (testCaseIndex == 4) {
      assertThat(result).isEmpty();
    }
  }
}
