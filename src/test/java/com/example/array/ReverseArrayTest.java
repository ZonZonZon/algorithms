package com.example.array;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ReverseArrayTest {

  @ParameterizedTest
  @CsvSource({"0,'1,2,3,4,5'", "1,'22,55,444,19,0'"})
  void array_OfIntegers_IsReversed1(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = ReverseArray.reverseArray1(intArray);
    if (testCaseIndex == 0) {
      assertThat(result[0]).isEqualTo(5);
      assertThat(result[1]).isEqualTo(4);
      assertThat(result[2]).isEqualTo(3);
      assertThat(result[3]).isEqualTo(2);
      assertThat(result[4]).isEqualTo(1);
    }
    if (testCaseIndex == 1) {
      assertThat(result[0]).isZero();
      assertThat(result[1]).isEqualTo(19);
      assertThat(result[2]).isEqualTo(444);
      assertThat(result[3]).isEqualTo(55);
      assertThat(result[4]).isEqualTo(22);
    }
  }

  @ParameterizedTest
  @CsvSource({"0,'1,2,3,4,5'", "1,'22,55,444,19,0'"})
  void array_OfIntegers_IsReversed2(int testCaseIndex, String intArguments) {
    int[] intArray =
        asList(intArguments.split(",")).stream()
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = ReverseArray.reverseArray2(intArray);
    if (testCaseIndex == 0) {
      assertThat(result[0]).isEqualTo(5);
      assertThat(result[1]).isEqualTo(4);
      assertThat(result[2]).isEqualTo(3);
      assertThat(result[3]).isEqualTo(2);
      assertThat(result[4]).isEqualTo(1);
    }
    if (testCaseIndex == 1) {
      assertThat(result[0]).isZero();
      assertThat(result[1]).isEqualTo(19);
      assertThat(result[2]).isEqualTo(444);
      assertThat(result[3]).isEqualTo(55);
      assertThat(result[4]).isEqualTo(22);
    }
  }
}
