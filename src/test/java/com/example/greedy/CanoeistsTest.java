package com.example.greedy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CanoeistsTest {

  @ParameterizedTest
  @CsvSource({
      "'60,45,28,90,36,54,70',100,4"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int maxLoad, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = Canoeists.getMinCoinsCombination(intArray, maxLoad);
    assertThat(result).isEqualTo(expected);
  }
}
