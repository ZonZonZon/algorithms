package com.example.dynamic;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CoinsExchangeTest {

  @ParameterizedTest
  @CsvSource({
      "'1,5,10,25',24,6",
      "'1,5,7',24,4"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, int sum, int expected) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = CoinsExchange.getMinCoinsCombination(intArray, sum);
    assertThat(result).isEqualTo(expected);
  }
}
