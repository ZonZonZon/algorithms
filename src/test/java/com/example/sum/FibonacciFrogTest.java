package com.example.sum;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FibonacciFrogTest {

  @ParameterizedTest
  @CsvSource({
      "'0,0,0,1,1,0,1,0,0,0,0',3",
      "'0,0,0,0,0,0,0,0,0,0,0',-1",
      "'0,0,0,1,0,1,1,0,1,1,1,1,0,1,1,1',-1",
      "'',1",
      "'1',2"
  })
  void aLimitingNumber_FibonacciSequence_IsReturned(String givenArguments, int expected) {

    int[] given =
        asList(givenArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = FibonacciFrog.getFibonacciFrogJumps(given);
    assertThat(result).isEqualTo(expected);
  }
}
