package com.example.sum;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import com.example.division.CommonPrimeDivisors;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FibonacciNumbersTest {

  @ParameterizedTest
  @CsvSource({
      "100,'0,1,1,2,3,5,8,13,21,34,55,89'"
  })
  void aLimitingNumber_FibonacciSequence_IsReturned(int limit, String expectedArguments) {

    int[] expected =
        asList(expectedArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = FibonacciNumbers.getFibonacciNumbers(limit);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
      "10,55"
  })
  void aNumberOrder_RespectiveFibonacciNumber_IsReturned(int numberOrder, int expected) {
    int result = FibonacciNumbers.getNthFibonacciNumber(numberOrder);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
      "2,true",
      "5,true",
      "55,true",
      "56,false"
  })
  void aNumber_AsTwoFibonacciNumbersSum_IsVerified(int number, boolean expected) {
    boolean result = FibonacciNumbers.isFibonacciNumbersSum(number);
    assertThat(result).isEqualTo(expected);
  }
}
