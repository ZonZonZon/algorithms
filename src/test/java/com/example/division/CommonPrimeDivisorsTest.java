package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CommonPrimeDivisorsTest {

  @ParameterizedTest
  @CsvSource({
      "'15,10,3','75,30,5',1",
      "'1','1',1",
//      "'2147483647','2147483647',1",
//      "'1','2147483647',1"
  })
  void twoNumbers_CommonPrimeDivisors_AreDefined(
      String argumentsA, String argumentsB, int expected) {

    int[] intArrayA =
        asList(argumentsA.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] intArrayB =
        asList(argumentsB.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int result = CommonPrimeDivisors.getCommonPrimeDivisors(intArrayA, intArrayB);
    assertThat(result).isEqualTo(expected);
  }
}
