package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ChocolatesByNumbersTest {

  @ParameterizedTest
  @CsvSource({
      "10,4,5",
      "24,18,4",
      "1,1,1",
      "23,1000000000,23",
      "1000000000,23,1000000000",
      "1000000000,1000000000,1"
  })
  void chocolates_NumberOfChocolatesToEat_IsDefined(
      int chocolatesCount, int toSkipCount, int expected) {

    int result = ChocolatesByNumbers.getNumberOfChocolatesToEat(chocolatesCount, toSkipCount);
    assertThat(result).isEqualTo(expected);
  }
}
