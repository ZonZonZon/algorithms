package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RectangleMinPerimeterTest {

  @ParameterizedTest
  @CsvSource({"36,24", "48,28", "30,22", "1,4", "2,6", "1000000000,126500"})
  void number_FactorizationApplied_PrimeFactorsAreReturned(int number, int expected) {

    int result = RectangleMinPerimeter.getRectangleMinPerimeter(number);
    assertThat(result).isEqualTo(expected);
  }
}
