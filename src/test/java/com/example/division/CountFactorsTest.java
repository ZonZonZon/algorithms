package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CountFactorsTest {

  @ParameterizedTest
  @CsvSource({
      "36,9",
      "24,8",
//      "2147483647,1",
      "2147483646,192",
      "1,1",
  })
  void number_FactorCombinationsCounted_AreReturned(int number, int expected) {

    int result = CountFactors.countFactors(number);
    assertThat(result).isEqualTo(expected);
  }
}
