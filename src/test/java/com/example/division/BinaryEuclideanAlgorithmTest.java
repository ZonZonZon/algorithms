package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class BinaryEuclideanAlgorithmTest {

  @ParameterizedTest
  @CsvSource({
      "12,16,4",
      "343,287,7",
      "123,82,41"
  })
  void twoNumbers_GreatestCommonDivisor_IsReturned(
      int numberA, int numberB, int expected) {

    int result = BinaryEuclideanAlgorithm.getGreatestCommonDivisor(numberA, numberB, 1);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
      "6,8,24",
      "18,45,90",
      "75,60,300",
      "72,99,792",
      "396,180,1980"
  })
  void twoNumbers_LeastCommonMultiple_IsReturned(
      int numberA, int numberB, int expected) {

    int result = BinaryEuclideanAlgorithm.getLeastCommonMultiple(numberA, numberB);
    assertThat(result).isEqualTo(expected);
  }
}
