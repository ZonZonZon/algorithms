package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CountDivisorsTest {

  @ParameterizedTest
  @CsvSource({
      "'2',2",
      "'3',2",
      "'4',3",
      "'5',2",
      "'6',4",
      "'7',2"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      int number, int expected) {

    int result = CountDivisors.getDivisors(number);
    assertThat(result).isEqualTo(expected);
  }
}
