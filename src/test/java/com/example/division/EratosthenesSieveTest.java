package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class EratosthenesSieveTest {

  @ParameterizedTest
  @CsvSource({
      "'2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17','2,3,5,7,11,13,17'"
  })
  void consecutiveNumbers_EratosthenesSieveApplied_PrimeNumbersAreReturned(
      String intArrayArguments, String expectedArguments) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] expected =
        asList(expectedArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = EratosthenesSieve.getPrimeNumbers(17);
    assertThat(result).isEqualTo(expected);
  }
}
