package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PrimeFactorizationTest {

  @ParameterizedTest
  @CsvSource({
      "18,'2,3,3'",
      "9,'3,3'",
      "21,'3,7'",
      "22,'2,11'",
      "256,'2,2,2,2,2,2,2,2'",
      "17,'17'"
  })
  void number_FactorizationApplied_PrimeFactorsAreReturned(
      int number, String expectedArguments) {

    int[] expected =
        asList(expectedArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = PrimeFactorization.getPrimeFactors(number);
    assertThat(result).isEqualTo(expected);
  }
}
