package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.asList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CoinFlippersTest {

  @ParameterizedTest
  @CsvSource({
    "'1,2,3,4,5,6,7,8,9,10','2,3,5,6,7,8,10'"
  })
  void operations_QueueIsEmpty_OperationsCurrentCountIsReturned(
      String intArrayArguments, String expectedArguments) {

    int[] intArray =
        asList(intArrayArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] expected =
        asList(expectedArguments.split(",")).stream()
            .filter(string -> !string.equals(""))
            .mapToInt(intObject -> Integer.parseInt((String) intObject))
            .toArray();

    int[] result = CoinFlippers.getCoinsFlipped(intArray);
    assertThat(result).isEqualTo(expected);
  }
}
