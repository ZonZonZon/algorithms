package com.example.division;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class NumismatistsTest {

  @ParameterizedTest
  @CsvSource({
      "1,2,3,6",
      "10,25,50,50"
  })
  void threeCoinDenominations_CommonMinimalSum_IsReturned(
      int numberA, int numberB, int numberC, int expected) {

    int result = Numismatists.getCommonMinimalSum(numberA, numberB, numberC);
    assertThat(result).isEqualTo(expected);
  }
}
